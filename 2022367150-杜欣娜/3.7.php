<?

// 【布尔型】定义一个变量a，赋值为false
$a = false;
// 【字符串】定义一个变量username，赋值为“Mike”
$username = "Mikes";
// echo命令打印，显示
echo "the username is  ", $username, "<br>";
// 打印你好，今天是星期四
// echo "<h1>你\a好\n\r今天是\\疯狂星期四,v我\$50</h1><br>";

$a = 10;
echo "a的值是$a<br>";
echo "a的值是\$a<br>";
// ==是作比较
// = 赋值
if ($username == "Mike") {
    echo "Hi, Mike <br>";
    # code...
} else {
    echo "you are not Mike<br>";

}


if ($a == true) {
    echo "a is true";
} else {
    echo "a is false";
}

echo "<br>";
//整型
$n1 = 123;
echo "n1=", $n1, "<br>";
$n2 = 0;
echo "n2=", $n2, "<br>";
$n3 = -123;
echo "n3=", $n3, "<br>";
$n4 = 0123;
echo "n4=", $n4, "<br>";
$n5 = 0xFF;
echo "n5=", $n5, "<br>";
$n6 = 0b11111111;
echo "n6=", $n6, "<br>";
//浮点型
$pi = 3.1415926;
echo "pi=", $pi, "<br>";
$width = 3.3e4;
echo "width=", $width, "<br>";
$var = 3e-2;
echo "var=", $var, "<br>";

echo 3 ** 4;
// 数组
$arr1 = array(1, 2, 3, 4, 5, 6, 7, 8);
$arr2 = array("animals" => "dog", "color" => "red");
// 格式化代码 alt+shift+F
echo "<br>", $arr1[0];//1
echo "<br>", $arr1[7];//1
echo "<br>", $arr2["color"];//1

//=====================================================
// 对象
class Animal
{
    //基本属性
    var $name;
    var $age;
    var $weight;
    var $sex;
    function __construct($name)
    { // 构造函数
        $this->name = $name; // 将传入的参数赋值给属性 name
    }
    //基本方法
    public function run()
    {
        echo "haha,", $this->name, " is running<br>";
    }
    public function eat()
    {
        echo "haha,i am eating";
    }
}
$dog = new Animal("dog");
$cat = new Animal("cat");
$dog->run();
$cat->run();

// 定义一个英雄类
class hero
{
    //血量
    public $hp = 100;
    //姓名
    public $name;
    //攻击力
    public $attack;

    public $shield = 0.2;

    function __construct($name, $attack)
    {
        $this->name = $name;
        $this->attack = $attack;
    }

    // 攻击
    function attack($hero)
    {
        echo $this->name, "攻击了", $hero->name,"<br>";
        $hero->hp -= $this->attack;

        $hero->hp = $hero->hp - $this->attack*(1-$hero->shield);
        echo $hero->name,"的剩余血量：", $hero->hp,"<br>";
    }
}

$hero1 = new hero("瑟提",6);
$hero2 = new hero("鲁班",10);

$hero1->attack($hero2);
$hero2->attack($hero1);
$hero2->attack($hero1);
$hero2->attack($hero1);
$hero2->attack($hero1);
?>