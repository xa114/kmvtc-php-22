<?php
class vegetable {
    public $name;
    public $color;
    public $taste;
    public function __construct( $name, $color, $taste) {
        $this->name =$name;
        $this->color =$color;
        $this->taste =$taste;
    }
    public function getInfo(){
        echo"蔬菜的名称:" . $this->name . "<br>";
        echo"蔬菜的颜色:" . $this->color . "<br>";
        echo"蔬菜的味道:" . $this->taste . "<br>";
    }
    /** 
     * 析构方法，在对象销毁时自动调用
    */
    public function __destruct() {
        echo "再见:". $this->name."<br>";
        }
  }
$kugua =new vegetable('苦瓜','绿色', '苦');
$kugua->getInfo();
$tomato =new vegetable('西红柿','红色', '甜');
$tomato->getInfo();
?>