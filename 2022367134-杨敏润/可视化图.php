<div class="page">
  <h1 style="width:100%">我的小账本</h1>
  <div id="zhuzhuangtu" class="zhuzhuangtu"></div>
  <div id="bingtu" class="bingtu"></div>
  <div id="mixzhu" class="mixzhu"></div>
</div>

<script src="https://unpkg.com/@antv/g2/dist/g2.min.js"></script>
<script>
  const data2 = [
    { item: '事例一', count: 40, percent: 0.4 },
    { item: '事例二', count: 21, percent: 0.21 },
    { item: '事例三', count: 17, percent: 0.17 },
    { item: '事例四', count: 13, percent: 0.13 },
    { item: '事例五', count: 9, percent: 0.09 },
  ];
  const chart2 = new G2.Chart({
    container: 'bingtu',

  });

  chart2.coordinate({ type: 'theta', outerRadius: 0.8 });

  chart2
    .interval()
    .data(data2)
    .transform({ type: 'stackY' })
    .encode('y', 'percent')
    .encode('color', 'item')
    .legend('color', { position: 'bottom', layout: { justifyContent: 'center' } })
    .label({
      position: 'outside',
      text: (data2) => `${data2.item}: ${data2.percent * 100}%`,
    })
    .tooltip((data) => ({
      name: data2.item,
      value: `${data2.percent * 100}%`,
    }));

  chart2.render();


  const data1 = [
    { genre: 'Sports', sold: 275 },
    { genre: 'Strategy', sold: 115 },
    { genre: 'Action', sold: 120 },
    { genre: 'Shooter', sold: 350 },
    { genre: 'Other', sold: 150 },
  ];

  // 初始化图表实例
  const chart1 = new G2.Chart({
    container: 'zhuzhuangtu',

  });

  // 声明可视化
  chart1
    .interval() // 创建一个 Interval 标记
    .data(data1) // 绑定数据
    .encode('x', 'genre') // 编码 x 通道
    .encode('y', 'sold'); // 编码 y 通道

  // 渲染可视化
  chart1.render();


  const data3 = [
    { name: 'London', 月份: 'Jan.', 月均降雨量: 18.9 },
    { name: 'London', 月份: 'Feb.', 月均降雨量: 28.8 },
    { name: 'London', 月份: 'Mar.', 月均降雨量: 39.3 },
    { name: 'London', 月份: 'Apr.', 月均降雨量: 81.4 },
    { name: 'London', 月份: 'May', 月均降雨量: 47 },
    { name: 'London', 月份: 'Jun.', 月均降雨量: 20.3 },
    { name: 'London', 月份: 'Jul.', 月均降雨量: 24 },
    { name: 'London', 月份: 'Aug.', 月均降雨量: 35.6 },
    { name: 'Berlin', 月份: 'Jan.', 月均降雨量: 12.4 },
    { name: 'Berlin', 月份: 'Feb.', 月均降雨量: 23.2 },
    { name: 'Berlin', 月份: 'Mar.', 月均降雨量: 34.5 },
    { name: 'Berlin', 月份: 'Apr.', 月均降雨量: 99.7 },
    { name: 'Berlin', 月份: 'May', 月均降雨量: 52.6 },
    { name: 'Berlin', 月份: 'Jun.', 月均降雨量: 35.5 },
    { name: 'Berlin', 月份: 'Jul.', 月均降雨量: 37.4 },
    { name: 'Berlin', 月份: 'Aug.', 月均降雨量: 42.4 },
  ];

  const chart3 = new G2.Chart({
    container: 'mixzhu',
  });

  chart3
    .interval()
    .data(data3)
    .encode('x', '月份')
    .encode('y', '月均降雨量')
    .encode('color', 'name')
    .transform({ type: 'stackY' })
    .interaction('elementHighlight', { background: true });

  chart3.render();


</script>

<style>
  .page {
    height: 100%;
    width: 100%;
    display: flex;
    flex-direction: row;
    flex-wrap: wrap;
  }

  .zhuzhuangtu {
    width:50%;
    display: flex;
    flex-direction: row;
    justify-content: center;
    align-items: center;

  }

  .bingtu {
    width:50%;
    display: flex;
    flex-direction: row;
    justify-content: center;
    align-items: center;
  }

  .mixzhu{
    width:100%;
    display: flex;
    flex-direction: row;
    justify-content: center;
    align-items: center;
  }
</style>