<?php
header("content-type:/html;charset=utf-8");
abstract class animal{
    abstract publicfunction shout();
}
class dog extends animal{
    public function shout() {
        echo "玩玩玩··········<br>";
    }
}
class cat extends animal{
    public function shout() {
        echo "喵喵·····<br>";
    }
}
$dog=new dog();
$dog->shout();
$cat=new cat();
$cat->shout();
?>
