<?php


//4.4.1 static 静态的
// 新建一个[动物]类
class animal {

    // ====================成员属性========================
    private $name;
    private $color;
    private $age;
    private static $sex = "雄性"; 
    // ====================成员属性========================
    // public function getInfo() {
    //     echo "动物的名称:". $this->name  . "<br>";
    //     echo "动物的颜色:". $this->color . "<br>";
    //     echo "动物的年龄:". $this->age   . "<br>";
    // }

    // 构造函数 
    public function __construct($name, $color, $age) {
        $this->name  = $name;
        $this->color = $color;
        $this->age   = $age;
    }

    private function getInfo()  {
        echo "动物是雄性的";   
    }
}

echo animal::$sex;
animal::getInfo();

