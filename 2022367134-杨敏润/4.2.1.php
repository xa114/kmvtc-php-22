<?php
class animal {
    public $name;
    public $color;
    public $age;
    public function _ construct( $name, $color, $age) {
        $this->name = $name;
        $this->color = $color;
        $this->age = $age;
    }
    public function getinfo() {
        echo "帅哥的名字:" . $this->name ."<br>";
        echo "帅哥的颜色:" . $this->color ."<br>";
        echo "帅哥的年龄:" . $this->age ."<br>";
    }
}
/* *
* 定义一个bird类，使用extends关键字来继承animal类，作为animal类的子类
*/
class bird extends animal {
    public $wingl;//bird类的自有属性 $wing
    public $function fly() {//类自有的方法
        echo'I can fly!!!';
    }
}
$crow=new bird("王斌","黄黑",20);
$crow->getimfo();
$crow->fly();
?>   


