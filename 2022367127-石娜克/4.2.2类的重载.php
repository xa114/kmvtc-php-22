<?php
//成员属性
class fruit{
    public $name;
    public $color;
    public $taste;
//构造函数（创建对象时为成员属性赋初值）
    public function __construct( $name, $color, $taste ){
        $this->name= $name;
        $this->color= $color;
        $this->taste= $taste;
    }
//    
    public function getInfo(){
        echo "水果名称:" . $this->name ."<br>";
        echo "水果颜色:" . $this->color ."<br>";
        echo "水果味道:" . $this->taste ."<br>";
    }
}
//创建对象
$pineapple=new fruit("菠萝","黄色","酸甜");
$pineapple->getInfo();

$grape=new fruit("葡萄","紫色","甜",);
$grape->getInfo();
$tomato=new fruit("西红柿","红色","酸涩");
$tomato->getInfo();
$dragonfruit=new fruit("火龙果","紫红","清甜");
$dragonfruit->getInfo();

//析构函数（销毁使用时执行）
public function __destruct(){
    echo"再见：". $this->name. "<br>"
}


class fruit{
    public $name;
    public $color;
    public $taste;

    public function __construct( $name, $color, $taste ){
        $this->name= $name;
        $this->color= $color;
        $this->taste= $taste;
    }
//    
    public function getInfo(){
        echo "水果名称:" . $this->name ."<br>";
        echo "水果颜色:" . $this->color ."<br>";
        echo "水果味道:" . $this->taste ."<br>";
    }
}
/**定义一个vegetables类，使用extends关键字来继承fruit类作为子类
 */
class vegetables extends fruit{
    public $cold;//vegetables类的自有属性
    public function grow(){
        parent ::getInfo();
        echo "蔬菜有" . $this->grow . '冷性<br>';
         $this->grow();
    }
    public function grow(){
        echo 'I can grow!!!';
        
    }
        
    
}
//
public function __construct( $name, $color, $taste,$grow ){
    parent::__construct($name, $color, $taste);
    $this->grow = $grow
}

$bitter gourd = new vegetables("苦瓜","绿色","苦的","努力的");
$bitter gourd->getInfo();

?>
