<?php
header("Conten-Type:text/html;charset=utf-8");
//使用abstract关键字声明一个抽象类
abstract class animal{
    //在抽象类中声明抽象方法
    abstract public function shout();
}
//定义dog类继承animal类
class dog extends animal {
    //实现抽象方法.
    public function shout(){
        echo "汪汪汪......<br>";
    }
}
//定义cat类继承animal类
class cat extends animal {
    //实现抽象方法
    public function shout(){
        echo"喵喵......<br>";
    }
}
$dog=new dog()
$dog->shout();
$cat=new cat();
$cat->shout();
?>
   
