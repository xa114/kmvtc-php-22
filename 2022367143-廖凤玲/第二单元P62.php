<?php
//gettype()
$a="Hello";
echo gettype($a)."<br>"; //输出结果：string
$b=array(1,2,5);
echo gettype( $b)."<br>";   //输出结果：array
//intval
echo intval(4.5)."<br>";     //输出结果：4
var_dump( $a);       //输出结果：string（5） “Hello”
echo "<br>";
var_dump( $a,$b);
//输出结果：string（5） “Hello” array(3) { [0]=>int(1) [1]=>int(2) [2]=>int(5)}
?>


<?php
echo(floor(0.60));   //输出结果：0
echo(floor(0.40));  // 输出结果：0
echo(floor(5));     //输出结果：5
echo(floor(5.1));      //输出结果：5
echo(floor(-5.1));     //输出结果：-6
echo(floor(-5.9));    //输出结果：-6
?>


<?php
echo rand();
echo rand();
echo rand(10,100);
?>


<?php
$pizza ="piece1 piece2 piece3 piece4 piece5 piece6";
$pieces = explode(" ", $pizza);
echo $pieces[0];  //输出 piece1
echo $pieces[1];    //输出 piece2
?>


<?php
echo md5("apple");    //输出结果：lf3870be274f6c49b3e31a0c6728957f
?>

<?php
var_dump(checkdate(12,31,2000));   //bool(true)
var_dump(checkdate(2,29,2001));     //bool(false)
?>


<?php
date_default_timezone_set("PRC");
var_dump(checkdate(12,31,2000))."<br>";   //输出结果：bool(true)
var_dump(checkdate(2,31,2000))."<br>";    //输出结果：bool(false)
echo time()."<br>";   //返回当前时间戳
echo mktime(0,0,0,12,25,2016)."<br>";   
//今天距2030年国庆节还有多少天
echo "今天距2030年国庆节还有". ceil((mktime(0,0,0,10,1,2030)-time())/(24*60*60))."天<br>"; 
//返回当前格式化的当地时间
echo "现在是：".date('Y-m-d H:i:s');
?>