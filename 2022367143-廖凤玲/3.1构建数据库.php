<?php

//mysqli_connect 用于连接数据库,返回一个连接对象
//mysqli_connect('数据库地址', '用户名', '密码', '数据库名', '端口', '套接字')
$conn = @mysqli_connect('localhost', 'bob',
    '123456', 'woodStars', '3306')
or die('Could not connect to MySQL: ' . mysqli_connect_error());

$select = mysqli_select_db($conn, 'woodStars');
if ($select) {
    echo "连接成功";
} else {
    echo "连接失败";
}

//$insert_sql = "insert into tb_admin (user, password, create_time, email)
//values ('Mary2', '123456', now(), 'Mary@qq.com')";

//mysqli_query 执行sql语句
//mysqli_query('连接对象', 'sql语句')
//$result = mysqli_query($conn, $insert_sql);

// 查询数据
$select_sql = "select * from tb_admin";
$result = mysqli_query($conn, $select_sql);// result 是函数返回的数据指针
//echo  $result;Fatal error: Uncaught Error: Object of class mysqli_result could not be converted to string in

//mysqli_fetch_array 从结果集中取得一行作为关联数组，或数字数组，或二者兼有
//mysqli_fetch_array('结果集', 'MYSQLI_ASSOC') MYSQLI_ASSOC 关联数组
//mysqli_fetch_array('结果集', 'MYSQLI_NUM') MYSQLI_NUM 数字数组
//mysqli_fetch_array('结果集', 'MYSQLI_BOTH') MYSQLI_BOTH 两者兼有
while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
    echo $row['user'] . ' ' . $row['password'] . ' ' . $row['create_time'] . ' ' . $row['email'] . '<br>';
}
//if ($result) {
//    echo "插入成功";
//
//
//} else {
//    echo "插入失败";
//}


// json 数组 ，包含多个json对象，json对象由多组键值对组成，一个json对象中，不能存在键值相同的数据
//[
//  {
//      "user": "Mary",
//    "password": "123456",
//    "create_time": "2024-06-13 20:12:06",
//    "email": "Mary@qq.com"
//  },
//  {
//      "user": "Mary2",
//    "password": "123456",
//    "create_time": "2024-06-13 20:13:15",
//    "email": "Mary@qq.com"
//  }
//]




//数据库名称为 library

// 创建一张表 图书主数据（） 包含 书名、书号、作者、出版社、出版时间、价格、ISBN、页数、分类、库存、简介
// 设置主键为 书号
// 创建一张表 图书借阅记录（） 包含 书号、借阅人、借阅时间、归还时间、借阅状态
// 设置主键为 自增主键
// 创建一张表 用户信息（） 包含 用户名、密码、邮箱、电话、地址、注册时间、用户类型
// 设置主键为 用户名

// 新增对应数据 每张表不少于5条


