<?php
class game
{
    public $name;
    public $type;
    public $age;
    public function __construct($name, $type, $age)
    {
        $this->name = $name;
        $this->type = $type;
        $this->age = $age;
    }
    public function getInfo()
    {
        echo "游戏的名称:" . $this->name . "<br>";
        echo "游戏的类型:" . $this->type . "<br>";
        echo "游戏的年龄:" . $this->age . "<br>";
    }
    public function __destruct()
    {
        echo "法克油 不肉:" . $this->name . "<br>";
    }
}
$csgo = new game('反恐精英', '射击', "20");
$csgo->getInfo();
$wallpaper = new game('壁纸', '背景', '3');
$wallpaper->getInfo();
?>  