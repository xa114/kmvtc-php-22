<?php

// 新建一个[动物]类
class jcc {

    // ====================成员属性========================
    public $name;
    public $chenyuan;
    public $age;

    // ====================成员属性========================
    public function getInfo() {
        echo "室友的名称:". $this->name  . "<br>";
        echo "室友的成员:". $this->chenyuan . "<br>";
        echo "室友的年龄:". $this->age   . "<br>";
    }

    // 构造函数 (创建对象时为对象赋初始值)
    // parent::__construct($name, $chenyuan, $age)
    public function __construct($name, $chenyuan, $age) {
        $this->name  = $name;
        $this->color = $chenyuan;
        $this->age   = $age;
    }

    // 析构函数 (销毁对象时执行)
    public function __destruct()
    {
        echo "再见:" . $this->name . "。<br>";
    }
}


// 类的继承，新建一个[人类]子类继承动物类
class hereo extend chenyuan{
    // 成员属性：人类
    public $multply;
    // 重载：继承类的方法的继承写法[parent::]
    public function getInfo(){
        // 使用[parent::]来获取父类的成员方法或者属性
        parent::getInfo();
        // echo "游戏的名称:". $this->name  . "<br>";
        // echo "游戏的成员:". $this->color . "<br>";
        // echo "游戏的年龄:". $this->age   . "<br>";
        // 书写鸟类特有的属性，信息
        echo "人类有" . $this->hmultply . '繁衍<br>';
        $this->multply();
    }

    // 子类[hereo]具有的成员方法
    public function multply(){
        echo $this->name . "is flying...";
    }
 
    // 继承类的构造函数的继承写法[parent::]
    public function __construct($name, $color, $age, $wing) {
        parent::__construct($name, $color, $age)、
        // 人类特有的繁衍属性需要单独书写
        $this->multply = $multply;
    }
}


// 如何创建一个对象？
$human = new human("小小穆","白色","20");
$human = new human("小小牛","黑色","19");

// 类和对象的关系
// 类的实例化结果  ->对象
// 对一类对象的抽象->类