<?php
class TestClass{
    public $foo;
    public function _construct($foo){
        $this->=$foo;
    }
    //定义一个_toString()方法，返回一个成员属性$foo
    public function _toString(){
        return $this->$foo;
    }}
    $class=new TestClass('Hallo');
    //直接输出对象
    echo $class; //输出结果为'Hallo'
    ?>