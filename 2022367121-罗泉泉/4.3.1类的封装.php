<?php
/**
 * 定义类MyClass
 */
class MyClass {
    public    $public    = 'Public';        //定义公共属性$public
    //第一个public是访问修饰符；
    //第二个public是一个叫做public的成员属性，变量
    //第三个public是$public的成员属性的值
    protected $protected = 'Protected';     //定义保护属性$protected
    private   $private   = 'Private';      //定义私有属性$private
    function printHello()  {               //输出3个成员属性
        // 成员内部引用成员属性，无任何权限问题
        echo $this->$public . "<br>";
        echo $this->$private . "<br>";
        echo $this->$protected . "<br>";
    }
}
$obj=new MyClass( );                      //实例化当前类
echo $obj->public;                        //输出$public
//echo $obj->protected; //Fatal error: Cannot access protected property MyClass::$protected
//echo $obj->private; //Fatal error: Cannot access private property MyClass::$private
$obj->printHello( );                      //输出3个成员属性
/**
 *定义类MyClass2
 */
class MyClass2 extends MyClass {          //继承MyClass类
        //可以访问公共属性和保护属性，但是私有属性不可以访问
        protected $protected='Protected2';
        function printHello()  {
            echo $this->public;
            echo $this->private;
            echo $this->protected;
    }
}
$obj2=new MyClass2( );
echo $obj2->public;                 //输出$public
//echo $obj2->protected;            //Cannot access protected property MyClass2::$protected
echo $obj2->private;                //未定义
$obj2->printHello( );               //只显示公共属性和保护属性，不显示私有属性
?>