<?php

// 新建一个[植物]类
class plant {

    // ====================成员属性========================
    public $name;
    public $color;
    public $age;
    public function __construct($name, $color, $age) {
        $this->name  = $name;
        $this->color = $color;
        $this->age   = $age;
    }
    public function getInfo() {
        echo "植物的名称:". $this->name  . "<br>";
        echo "植物的颜色:". $this->color . "<br>";
        echo "植物的年龄:". $this->age   . "<br>";
    }
}
/**
 * 定义一个flower类，使用extends关键字来继承plant类，作为plant类的子类
 */
class flower extends plant {
    public $aroma;//flower类自有的属性$aroma
    public function getInfo() {
        parent::getInfo();
        echo"花类有" . $this->aroma . '香味<br>';
        $this-> medical();
}
    public function medical() {
        //花类自有的方法
        echo "我可以药用！！！";
    }
    public function __construct($name, $color, $age,$special) {
        parent::__construct($name, $color, $age);
        $this->aroma=$aroma;
    }
}
$crow=new rose("玫瑰","白色",4,"特别的");
$crow->getInfo();
?>