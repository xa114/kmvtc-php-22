<?php
class animal {
      public $name;
      public $color;
      public $age;
      public function __construct($name, $color, $age) {
        $this->name = $name;
        $this->color = $color;
        $this->age = $age;
      }
      public function getName() {
        echo "动物的名称：" . $this->name . "<br>";
        echo"动物的颜色：" . $this->color . "<br>";
        echo "动物的年龄：" . $this->age ."<br>";
      }
}
/**
 * 定义一个bird类，使用extends关键字来继承animal类，作为animal类的子类
 */
class bird extends animal {
    public $wing;//bird类的自由属性 $wing
    public function fly() {
        echo'I can fly!!! ';
    }
}
$crow = new bird("乌鸦","黑色",3);
$crow->getInfo();
$crow->fly();

class animal {
    public $name;
    public $color;
    public $age;
    public function __construct($name, $color, $age) {
        $this->name = $name;
        $this->color = $color;
        $this->age = $age;
    }
    public function getInfo() {
        echo"动物的名称:" . $this->name . "<br>";
        echo "动物的颜色:" . $this->color . "<br>";
        echo "动物的年龄:" . $this->age . "<br>";
    }
}
/** 定义一个bird类，使用extends关键字来继承animal类，作为animal类的子类
 */
class bird extends animal {
    public $wing;
    public function getInfo() {
        parent::getInfo();
        echo "鸟类有" . $this->wing . '翅膀<br>';
        $this->fly() ;
    }
    public function fly() {
        echo "我会飞翔!!!";
    }
    public function __construct($name, $color, $age,$swing) {
        parent::__construct($name, $color, $age);
        $this->wing = $swing;
    }
}
$crow= new bird("乌鸦","黑色","漂亮的",3);
$crow->getInfo();
?>