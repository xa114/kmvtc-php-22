<?php
header("Content-Type:text/html;charset=utf-8")
class phone {
    private $name;        //私有属性name;
    private $color;       //私有属性color;
    private $moder;       //私有属性moder;
    public function __construct($name, $color, $moder) {
        $this->name = $name;
        $this->color = $color;
        $this->moder = $moder;
    }
    public function getInto() {
        echo"手机的名称" . $this->name . "<br>";
        echo"手机的颜色" . $this->color . "<br>";
        echo"手机的型号" . $this->moder . "<br>";
    }
}
$HUAWEI=new phone("P60","白色","PRO");
//$HUAWEI->getInfo();
//回看浏览器回显报错下属代码在类的外部尝试换取phone类的实体化HUAWEI对类的name属性
echo $HUAWEI->name;
?>
