<?php

interface anima {     //在定义接口时，需使用interface关键字
    function run();
    function shout();
}

header ("Content-Type:text/html;charset=utf-8");
//定义接口
interface animal {
    //声明抽象方法
    function run();
    function shout();
}
//定义dog类，实现animal 接口
class dog implements animal {
//实现接口中的抽象方法
      public function run() {
        echo "小狗在奔跑<br>";
      }
      public function shout() {
        echo "汪汪汪<br>"
      }
}
//定义cat类，实现animal 接口
class cat implements animal {
    //实现接口中的抽象方法
          public function run() {
            echo "小猫在奔跑<br>";
          }
          public function shout() {
            echo "喵喵喵<br>"
          }
    }
$dog=new dog();
$dog->run();
$dog->shout();
$cat=new cat();
$cat->run();
$cat->shout();

header ("Content-Type:text/html;charset=utf-8");
//定义接口
interface animal {
    //声明抽象方法
    function run();
    function shout();
}
//定义landanimal接口
interface landanimal {
    public function liveonland();
}
//定义dog类，实现animal接口和landanimal接口
class dog implements animal ,landanimal {
    //实现接口中的抽象方法
    public function run() {
        echo "小狗在奔跑<br>";
      }
    public function shout() {
        echo "汪汪汪<br>"
      }
    public function liveonland() {
        echo "小狗在陆地上生活<br>";
    }
}
$dog=new dog();
$dog->run();
$dog->shout();
$dog->liveonland();
?>