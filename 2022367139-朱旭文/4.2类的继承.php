<?php
class animal {
    public $name;
    public $color;
    public $age;
    public function __construct( $name, $color, $age) {
        $this->name= $name;
        $this->color= $color;
        $this->age= $age;
    }
    //构造函数（创建对象时为成员属性赋初值，完成初始化工作）
    public function getInfo() {
        echo "恐龙的大小:" . $this->size . "<br>";
        echo "恐龙的颜色:" . $this->color . "<br>";
        echo "恐龙的年龄:" . $this->age . "<br>";
    }
    /* *
     * 析构方法，在对象销毁时自动调用
     */
    public function __destruct() {
        echo "再见:" . $this->name. "<br>";
    }
    $konglong new animal('80m','五彩斑斓'，'88');
    $konglong->getInfo();
    $konglong new animal('90m','黑色'，'99');
    $konglong->getInfo();

    //定义一个brid类，使用extends关键字来继承animal类，作为animal类的子类
    class brid extends animal {
        public $wing;//brid类自有的属性$wing
        public function getInfo() {
            parent::getInfop();
            echo "恐龙有" . $this->wing . '嘴<br>';
            $this->fly();
        }
        public function __construct( $name, $color, $age,$swibg) {
            parent::__construct( $name, $color, $age);
            $this->wing=$wing;
        }
    }
    $crow=new bird("100","蓝色"，4,"大")；
    $crow->getInfo();
    
    ?>