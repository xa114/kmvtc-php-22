<?php
//gettype()
$a="Hello";
echo gettype($a). "<br>"; //输出结果：string
$b=array(1,2,5);
echo gettype($b). "<br>"; //输出结果：array
//intval
echo intval(4.5). "<br>"; //输出结果：4
//var_dump
var_dump($a);  //输出结果：string(5) "Hello"

echo "<br>";
var_dump($a,$b);
//输出结果：string(5) "Hello" array(3) { [0]=> int(1) [1]=> int(2) [2]=> int(5) }


echo "==================================================================","<br>";


echo (floor(0.60))."<br>";
echo (floor(0.40))."<br>";
echo (floor(5))."<br>";
echo (floor(5.1))."<br>";
echo (floor(-5.1))."<br>";
echo (floor(-5.9))."<br>";


echo "==================================================================","<br>";

echo rand()."<br>";
echo rand()."<br>";
echo rand(10,100)."<br>";


echo "==================================================================","<br>";

$pizza = "piece1 piece2 piece3 piece4 piece5 piece6 ";
$pieces = explode (" ",$pizza);
echo $pieces[0]."<br>";
echo $pieces[1]."<br>";


echo "==================================================================","<br>";

echo md5("apple");
echo "<br>";

echo "==================================================================","<br>";

var_dump(checkdate(12,31,2000));
echo "<br>";
var_dump(checkdate(2,29,2001));
echo "<br>";


echo "==================================================================","<br>";

date_default_timezone_set("PRC");
var_dump(checkdate(12,31,2000));
var_dump(checkdate(2,31,2000));

echo time()."<br>";
echo mktime(0,0,0,12,25,2016)."<br>";
echo "今天距2030年国庆还有".ceil((mktime(0,0,0,10,1,2030) -time())/(24*60*60))."天<br>";
echo "现在是：".date('Y-m-d H:i:s');

echo "==================================================================","<br>";

function random_text( $count, $rm_similar = false) { 

   $chars = array_flip( array_merge( range(0,9),range('A','Z')));
   if ( $rm_similar ) {
       unset( $chars[0],$chars[1],$chars[2],$chars['I'],$chars['O'],$chars['Z']);
   }
   for ( $i = 0, $text =''; $i < $count; $i++ ) {
       $text =array_rand( $chars);
   }
   return $text;
}

include'functions.php';
echo random_text(5,true);



   ?>