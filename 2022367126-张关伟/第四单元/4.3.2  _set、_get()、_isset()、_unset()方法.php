<?php
//__get、__set方法
header("content-type:text/html;charset=utf-8");
class plant {
    private $name;
    private $color;
    private $floresccence;
    //__get()方法用来获取设置私有属性的值
    public function __get($protecty_name){
        if(isset($this->$protecty_name)){
            return $this->$protecty_name;
        }else{
            return (null);
        }
    }
    //__set()方法用来获取设置私有属性的值
    public function __set($protecty_name, $value){
        $this->$protecty_name = $value;
    }
}
$flower=new plant();
$flower->name = "玫瑰";
$flower->color = "粉色";
$flower->floresccence = "15天";
echo $flower->name."<br>";
echo $flower->color."<br>";
echo $flower->floresccence."<br>";





echo "==============================================================================================="."<br>";

//__isset()、__unset()方法
class Electronics {
    private $name;          //私有的成员属性
    private $model;         
    private $brand;         
    function __get($protecty_name){
        if(isset($this->$protecty_name)){
            return ($this->$protecty_name);
        }else{
            return (null);
        }
    }
    // __set()方法用来设置私有属性
    function __set($protecty_name, $value){
        $this->$protecty_name = $value;
    }

    //__isset()方法
    function __isset($protecty_name){
        return isset($this->$protecty_name);
    }

    //__unset()方法
    function __unset($protecty_name){
        unset($this->$protecty_name);
    }
}
$phone = new Electronics();
$phone->name = "华为Pura 70"."<br>";
echo var_dump(isset( $phone->name) )."<br>";
//调用__isset()方法，输出：bool（true）
echo $phone->name."<br>";
unset( $phone->name ); //调用__unnset()方法
echo $phone->name;  //无输出


?>
