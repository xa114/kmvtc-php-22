<?php
// 使用private关键字来对属性和方法进行封装。
// 原来的成员如下：
//  var $nmae ;              //声明动物的称呼
//  var $color;              //声明动物的颜色
//  var $age  ;              //声明动物的年龄
//  function getinfo(){...}
// 改成封装形式后如下：
//  private $name ;           //把动物的称呼使用private关键字进行封装
//  private $color;           //把动物的颜色使用private关键字进行封装
//  private $age  ;           //把动物的年龄使用private关键字进行封装
//  function getinfo(){...} 

header("content-type:text/html;charset=utf-8");
class pig {
    private $name ;
    private $color;
    private $age ;
    function __construct($name, $color, $age) {
        $this->name = $name;
        $this->color = $color;
        $this->age = $age;
    }
    public function getinfo() {
        echo "猪的名称:".$this->name."<br>";
        echo "猪的颜色".$this->color."<br>";
        echo "猪的年龄".$this->age."<br>";
    }
}
$pig =new pig("佩奇","粉丝","5");
//$pig->getinfo();
echo $pig->name();





echo "========================================================================================================"."<br>";









/**
 * 定义类
 */
class Myclass{
    public $public='Public';                  //定义公共属性$public
    protected $protected='Protected';         //定义保护属性$protected
    private $private='Private';               //定义私有属性$private
    function printHello(){                    //输出3个成员属性
        echo $this->public;          
        echo $this->protected;
        echo $this->private;
    }
}
$obj=new MyClass();
echo $obj->public;
// echo $obj->protected;Fatal error:Cannot access protected property Myclass::$protected
// echo $obj->private;Fatal error:Cannot access private property Myclass::$private
$obj->printHello();

/**
 * 定义类Myclass2
 */
class Myclass2 extends MyClass {                  //继承MyClass类
        //可以访问公共属性和保护属性，但是私有属性不可以访问
        protected $protected='Protected';
        function printHello() {
            echo $this->public;
            echo $this->protected;
            echo $this->private;
        }
    }
$obj2=new MyClass2();
echo $obj2->public;                       //输出$public
//echo $obj2->protected;                  //Cannot access protected property Myclass::$protected
echo $obj2->private;                      //未定义
$obj2->printHello();                      //只显示公共属性和保护属性，不显示私有属性

?>