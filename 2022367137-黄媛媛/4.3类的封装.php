<?php

// 新建一个[蔬菜]类
class vegetable {

    // ====================成员属性========================
    private $name;
    private $color;
    private $taste;

    // ====================成员属性========================
    public function getInfo() {
        echo "蔬菜的名称:". $this->name  . "<br>";
        echo "蔬菜的颜色:". $this->color . "<br>";
        echo "蔬菜的味道:". $this->$taste   . "<br>";
    }

    // 构造函数 
    public function __construct($name, $color, $taste) {
        $this->name  = $name;
        $this->color = $color;
        $this->taste = $taste;
    }

}

$broccoli = new vegetable("西蓝花","颜色","味道");

// 需要同学查看浏览器回显报错，下属代码在类的外部尝试获取vegetable类的实体化broccoli对象的name属性
echo $broccoli->name;

//==============================================================================================
// 访问修饰符
// 1.public    公有的  无访问限制，不做特别说明，均默认为声明的是public，成员内部、外部均可访问
// 2.private   私有的  仅针对当前class享有访问权限，当前类的子类以及类的外部均不允许访问
// 3.protected 保护的  所属类及其子类享有访问权限，外部代码不可访问

class MyClass 
{
    public    $public    = 'Public';
    //第一个public是访问修饰符；
    //第二个public是一个叫做public的成员属性，变量
    //第三个public是$public的成员属性的值
    private   $private   = 'Private';
    protected $protected = 'Protected';


    function printHello()  {
        // 成员内部引用成员属性，无任何权限问题
        echo $this->$public . "<br>";
        echo $this->$private . "<br>";
        echo $this->$protected . "<br>";
    }
}

//4.3.2.1 get  (取值)
public function __get($property_name){
    // 检查属性 this-> 当前类  的 property_name 属性 是否存在
    if(isset($this->$property_name)){
        return (this->$property_name);
    }else {
        return(NULL);
    }

}
//4.3.2.2 set  (赋值)
public __set($property_name,$value){
    $this->$property_name = $value;
}

//4.3.2.3 isset  (检查)
private function __isset($property_name) {
    return isset($this->$property_name);
}

//4.3.2.4 unset  (删除)
private function __unset($property_name) {
    return unset($this->$property_name);
}
?>

<?php
class vegetale {
    private $name;
    private $color;
    private $taste;
    //__get()方法用来获取私有属性的值
    public function __get( $property_name) {
        if(isset( $property_name)) {
            return $this->$property_name;
        }else{
            return(NULL);
        }
    }
    //__set方法用来设置私有属性的值
    public function __set( $property_name, $value) {
        $this->$property_name= $value;

    }
}
$eggplant=new vegetable();
$eggplant->name="茄子";//自动调用__set()方法
$eggplant->color="紫色";
$eggplant->taste="甘甜";
echo $eggplant->name."<br>";//自动调用__get()方法
echo $eggplant->color."<br>";
echo $eggpalnt->taste."<br>";
?>




<?php
class vegetable {
    private $name;//私有的成员属性
    private $color;
    private $taste;
    //__get()方法用来获取私有属性
    function __get( $property_name) {
        if(isset( $this->$property_name)) {
            return( $this->$property_name);
        }else{
            return(NULL);
        }
    }
    //__set方法用来设置私有属性
    function__set( $property_name, $value) {
        $this->$property_name= $value;

    }
    //isset()方法
    function __isset( $property_name) {
        return isset( $this->$property_name);
    }

    //__unset方法
    function __unset( $property_name) {
        unset( $this->$property_name);
    }
}
$tomato=new vegetable();
$tomato->name="番茄";
echo var_dump(isset( $tomato->name)) . "<br>";
//调用__isset()方法，输出：bool(true)
echo $tomato->name . "<br>";
unset( $tomato->name);//调用__unset()方法
echo $tomato->name;//无输出
?>

