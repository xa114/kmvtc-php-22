<?php
class animal {
    public $name;
    public $color;
    public $age;
    public function __construct( $name, $color, $age) {
        $this->name= $name;
        $this->color= $color;
        $this->age= $age;
    }
    public function getInfo() {
        echo "动物的名称:" . $this->name . "<br>";
        echo "动物的颜色:" . $this->color . "<br>";
        echo "动物的年龄:" . $this->age . "<br>";
    }
    /* *
     * 析构方法，在对象销毁时自动调用
     */
    public function __destruct() {
        echo "嗨咯:" . $this->name. "<br>";
    }
}
$dog=new animal('小狗','棕色',2);
$dog->getInfo();
$sheep=new animal('小羊','白色',3);
$sheep->getInfo();
?>