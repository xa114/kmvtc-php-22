<?php
//4.4.1 static关键字
//新建一个动物类
class animal {
    private $name;
    private $color;
    private $age;
    private static $sex="雄性";//私有的静态属性
    //静态成员方法
    public static function getInfo() {
        echo "动物是雄性的。";
    }
}
//echo animal::$sex; //错误，静态属性私有，外部不能访问
animal::getInfo(); //调用静态方法
?>

<?php
//4.4.2final关键字
class animal {
    final public function _construct() {

    }
}
class bird extends animal{
    public function _construct(){
    }
}
$crow=new bird();
$crow->getInfo();
?>

<?php
//4.4.3关键字
class animal {
    private static $firstCry=0; //私有的静态变量
    private $lastCry;
    //构造方法
    function _construct() {
        $this->lastCry = ++self::$firstCry;
    }
    function printLastCry() {
        var_dump($this->lastCry);
    }
}
$bird=new animal(); //实例化对象
$bird->printLastCry(); //输出：int（1）
?>

<?php
//4.4.4关键字
class MyClass {
    //定义一个常量
    const constant='我是个常量！';
    function showConstant() {
        echo self::constant."<br>"; //类中访问常量
    }
}
echo MyClass::constant."<br>"; //使用类名来访问变量
$class=new MyClass();
$class->showConstant();
?>

<?php
//4.4.5 _toString()方法
class TestClass {
    public $foo;
    public function _construct( $foo) {
        $this->foo = $foo;
    }
    //定义一个_toString()方法，返回一个成员属性 $foo
    public function _toString() {
        return $this->foo;
    }
}
$class = new TestClass('Hello');
//直接输出对象
echo $class; //输出结果为'Hwlllo'
?>


<?php
//4.4.6 _clone()方法
class animal {
    private $name; //成员属性 name
    private $color;
    private $age;
    public function _construct( $name, $color, $age) {
        $this->name = $name;
        $this->color = $color;
        $this->age = $age;
    }
    public function getInfo() {
        echo'名字:' . $this->name . , '颜色:' . $this->color . ,'年龄:' . $this->age . '.';
    }
}
$pig = new animal('猪','白色','1岁')；
$pig2 = clone $pig; //克隆对象
$pig2->getInfo();
?>


<?php
//4.4.7 _call()方法
class Test {
    function _call( $function_name, $args) {
        print "你所调用的函数: $function_name(参数:";
        print_r( $args);
        print ")不存在! <br>";
    }
}
$test = new Test();
$test->demo("one","two","three");
?>


<?php
//4.4.8 自动加载类
spl_autoload_register(function ( $class_name) {
    require_once $class_name . '.class.php';
});
$obj = new YxClass1();
print_r( $obj);
$obj2 = new MyClass2();
print_r( $obj2);
?>
