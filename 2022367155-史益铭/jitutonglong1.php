<!DOCTYPE html>
<html>
<head>
    <title>鸡兔同笼问题</title>
    <meta charset="UTF-8">
</head>
<body>
    <table align="center" border="1" width="500">
        <caption><h1>鸡兔同笼问题</h1></caption> 
        <form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
            <label for="heads">头数:</label>
            <input type="number" name="heads" id="heads" required><br><br>
            <label for="feet">脚数:</label>
            <input type="number" name="feet" id="feet" required><br><br>
            <input type="submit" value="解决问题">
        </form>

        <?php
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            $heads = $_POST['heads'];
            $feet = $_POST['feet'];

        // 检查输入是否有效
        if ($heads < 1 || $feet < $heads * 2 || $feet > $heads * 4) {
        echo "<p>输入无效！请确保头数和脚数是合理的。</p >";
} else {
// 解决问题
$rabbits = ($feet - 2 * $heads) / 2;
$chickens = $heads - $rabbits;

echo "<p>计算结果为：鸡有 {$chickens} 只，兔子有 {$rabbits} 只。</p >";
}
}
?>
</body>
</html>
