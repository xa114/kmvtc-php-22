<?php
// __call() 方法,当调用的方法不存在或者不可见时，__call() 方法会被调用
class callTest{
    //$name 参数是要调用的方法名称
    //$arguments 参数是一个数组，包含了要传递给方法的参数
    public function __call($function_name, $arguments) {
        print "你所调用的函数：'$function_name' 不存在！\n";
        print "你所传递的参数是：\n";
        print_r($arguments);
    }

//    public function runTest($a, $b, $c): void
//    {
//        echo "in runTest\n";
//        echo $a . "\n";
//        echo $b . "\n";
//        echo $c . "\n";
//    }
}

$obj = new callTest();
$obj->runTest('in runTest', '1','a','ccc');

//你所调用的函数：'runTest' 不存在！
//你所传递的参数是：
//Array
//(
//    [0] => in runTest
//    [1] => 1
//    [2] => 2
//    [3] => 3
//)