<?php

// 接口
interface humans
{
    // 接口中的方法不能有方法体,只能有方法名和参数
    function run();
    function shout();
}

// 呼吸系统
interface breath
{
    // 呼吸
    public function breathe();
}


class chinese implements humans, breath
{
    function run(): void
    {
        echo "中国人在跑";
    }
    function shout(): void
    {
        echo "中国人在叫";
    }
    function breathe(): void
    {
        echo "中国人在优雅的呼吸";
    }
}

// 日本人
class japanese implements humans
{
    function run(): void
    {
        echo "日本人在跑";
    }
    function shout(): void
    {
        echo "日本人在叫";
    }
    function breathe(): void
    {
        echo "日本人在普通的呼吸";
    }
}

// 实例化chinese类
$chinese = new chinese();
// 调用chinese类的run方法
$chinese->run();
echo "\n";
// 调用chinese类的shout方法
$chinese->shout();
echo "\n";
// 调用chinese类的breathe方法
$chinese->breathe();


// 实例化japanese类
$japanese = new japanese();
// 调用japanese类的run方法
$japanese->run();
echo "\n";
// 调用japanese类的shout方法
$japanese->shout();
echo "\n";
// 调用japanese类的breathe方法
$japanese->breathe();