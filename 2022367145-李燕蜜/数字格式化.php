<div class="page">
  <h1 style="width:100%">我的小账本</h1>
  <div id="zhuzhuangtu" class="zhuzhuangtu"></div>
  <div id="bingtu" class="bingtu"></div>
  <div id="mixzhu" class="mixzhu"></div>
  <div id="tiaoxingtu" class="tiaoxingtu"></div>

</div>
<script src="https://unpkg.com/@antv/g2/dist/g2.min.js"></script>
<script>
  const data2 = [
    { item: '美团', count: 40, percent: 0.4 },
    { item: '淘宝', count: 21, percent: 0.21 },
    { item: '微信', count: 17, percent: 0.17 },
    { item: '支付宝', count: 13, percent: 0.13 },
    { item: '转账', count: 15, percent: 0.09 },
  ];
  const chart2 = new G2.Chart({
    container: 'bingtu',

  });

  chart2.coordinate({ type: 'theta', outerRadius: 0.8 });

  chart2
    .interval()
    .data(data2)
    .transform({ type: 'stackY' })
    .encode('y', 'percent')
    .encode('color', 'item')
    .legend('color', { position: 'bottom', layout: { justifyContent: 'center' } })
    .label({
      position: 'outside',
      text: (data2) => `${data2.item}: ${data2.percent * 100}%`,
    })
    .tooltip((data) => ({
      name: data2.item,
      value: `${data2.percent * 100}%`,
    }));

  chart2.render();


  const data1 = [
    { genre: 'daily necessities', sold: 100 },
    { genre: 'food', sold: 500 },
    { genre: 'clothes', sold: 100 },
    { genre: 'fruit', sold: 100 },
    { genre: 'Other', sold: 200 },
  ];

  // 初始化图表实例
  const chart1 = new G2.Chart({
    container: 'zhuzhuangtu',

  });

  // 声明可视化
  chart1
    .interval() // 创建一个 Interval 标记
    .data(data1) // 绑定数据
    .encode('x', 'genre') // 编码 x 通道
    .encode('y', 'sold'); // 编码 y 通道

  // 渲染可视化
  chart1.render();


  const data3 = [
    { name: 'outlay', 月份: 'Jan.', 月花销: 1000 },
    { name: 'outlay', 月份: 'Feb.', 月花销: 1000},
    { name: 'outlay', 月份: 'Mar.', 月花销: 1000 },
    { name: 'outlay', 月份: 'Apr.', 月花销: 1000 },
    { name: 'outlay', 月份: 'May',  月花销: 1000 },
    { name: 'outlay', 月份: 'Jun.', 月花销: 1000 },
    { name: 'outlay', 月份: 'Jul.', 月花销: 1000 },
    { name: 'outlay', 月份: 'Aug.', 月花销: 1000 },
    { name: 'revenue', 月份: 'Jan.', 月花销: 1500 },
    { name: 'revenue', 月份: 'Mar.', 月花销: 1500 },
    { name: 'revenue', 月份: 'Apr.', 月花销: 1500 },
    { name: 'revenue', 月份: 'May',  月花销: 1500 },
    { name: 'revenue', 月份: 'Jun.', 月花销: 1500 },
    { name: 'revenue', 月份: 'Jul.', 月花销: 1500 },
    { name: 'revenue', 月份: 'Aug.', 月花销: 1500 },
  ];

  const chart3 = new G2.Chart({
    container: 'mixzhu',
  });

  chart3
    .interval()
    .data(data3)
    .encode('x', '月份')
    .encode('y', '月花销')
    .encode('color', 'name')
    .transform({ type: 'stackY' })
    .interaction('elementHighlight', { background: true });

  chart3.render();


</script>

<style>
  .page {
    height: 90%;
    width: 100%;
    display: flex;
    flex-direction: row;
    flex-wrap: wrap;
  }

  .zhuzhuangtu {
    width:50%;
    display: flex;
    flex-direction: row;
    justify-content: center;
    align-items: center;

  }

  .bingtu {
    width:50%;
    display: flex;
    flex-direction: row;
    justify-content: center;
    align-items: center;
  }

  .mixzhu{
    width:100%;
    display: flex;
    flex-direction: row;
    justify-content: center;
    align-items: center;
  }
  const chart = new Chart({
  container: 'tiaoxingtu',
  autoFit: true,
});
chart
  .interval()
  .coordinate({ transform: [{ type: 'transpose' }] })
  .data(data)
  .encode('x', 'year')
  .encode('y', 'sales');

chart.render();

  
</style>