<?php
//新建一个[水果]类
class fruit {
    //构造函数
    private $name;//私有属性name
    private $color;//私有属性color
    private $age;//私有属性age
    public function__construct($name, $color, $age) {
        $this->name  = $name;
        $this->color = $color;
        $this->age   = $age;
    $plum = new fruit("李子","红色","10天");
    $fig = new fruit("无花果","绿色","3天");
    }
    public function getInfo() {
        echo "水果的名称:". $this->name  . "<br>";
        echo "水果的颜色:". $this->color . "<br>";
        echo "水果的年龄:". $this->age   . "<br>";
    }
}
$plum= new fruit("李子","红色","10天");
echo $dog->name;



/**
*定义类Myclass
*/
class MyClass {
    public    $public    = 'Public';   //定义公共属性$public
    //第一个public是访问修饰符；
    //第二个public是一个叫做public的成员属性，变量
    //第三个public是$public的成员属性的值
    protected $protected = 'Protected';//定义保护属性$protected\
    private   $private   = 'Private';//定义私有属性$private
    function printHello()  {    //输出3个成员属性
    // 成员内部引用成员属性，无任何权限问题
    echo $this->$public . "<br>";
    echo $this->$private . "<br>";
    echo $this->$protected . "<br>";
   }
}
$obj = new MyClass(); //实例化当前类
echo  $obj->public; //输出$public
//echo $obj->protectedl;//Fatal error: Cannot access protected property Myclass::$protected
//echo $obj->private; //Fatal error: Cannot access private property Myclass::$private 
$myClass->printHello();//输出3个成员属性
// 在成员外部进行成员属性的直接访问，访问修饰符起到权限判断的作用
echo $myClass->public;
echo $myClass->private;
echo $myClass->protected;
/**
 * 定义类Myclass2
 */
 //子类中的访问修饰符========================
 class MyClass2 extend MyClass {  //继承Myclass类
    //可以访问公共属性和保护属性，但是私有属性不可以访问
    protected $protected = 'Protected2';

    function printHello()  {
        // 成员内部引用成员属性，无任何权限问题
        echo $this->$public . "<br>";
        echo $this->$private . "<br>";    // 不显示
        echo $this->$protected . "<br>";
    }
}
$obj2 = new MyClass2();
echo $obj2->public;  //输出$public
echo $obj2->private;  //未定义
echo $myClass2->protected;
$obj2->printHello(); //只显示公共属性和保护属性，不显示私有属性
?>










