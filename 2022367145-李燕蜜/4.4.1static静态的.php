<?php

//4.4.1 static关键字 静态的
class fruit {
    //构造函数
    private $name;
    private $color;
    private $age;
    pprivate static $taste="甜的"
    public function__construct($name, $color, $age) {
        $this->name  = $name;
        $this->color = $color;
        $this->age   = $age;
    $plum = new fruit("李子","红色","10天");
    $fig = new fruit("无花果","绿色","3天");
    }
    public function getInfo() {
        echo "水果的名称:". $this->name  . "<br>";
        echo "水果的颜色:". $this->color . "<br>";
        echo "水果的年龄:". $this->age   . "<br>";
    }
}
//构造函数
public function__construct($name, $color, $age) {
    $this->name  = $name;
    $this->color = $color;
    $this->age   = $age;

    private static $taste="甜的"//私有的静态属性
    //静态成员的方法
    public static function getInfo() {
        echo "水果是甜的。"；
    }
}
//echo fruit::$taste;  //错误，静态属性私有，外部不能访问
fruit::getInfo(); //调用静态方法 
$plum=new fruit("李子","红色","10天");
$plum->getInfo();


echo fruit::$taste;//静态成员不能在外部访问
echo fruit::$taste;//只有static属性可以通过类来访问，
fruit::grtInfo();//使用静态方法
?>