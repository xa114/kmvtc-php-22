abstract class 类别{
    //类的成员
}
<?php
header("Content-Type:text/html;charset=utf-8");
//使用abstract关键字声明一个抽象类
abstract class animal{
    //在抽象类中声明抽象方法
    abstract public function shout();

}
//定义dog类继承animal
class dog extends animal{
    public function shout(){
        echo"汪汪......<br>";

    }
}
class cat extends animal{
    public function shout(){
        echo "喵喵.......<br>";

    }
}
$dog=new dog();
$dog->shout();
$cat=new cat();
$cat->shout();
?>
4.6接口
interface animal{
    function run();
    function shout();

}
<?php
header("Content-Type:text/html;charset=utf-8");
interface animal{
    function run();
    function shout();

}
class dog implements animal{
    public function run(){
        echo "小狗在奔跑<br>";

    }
    public function shout(){
        echo "汪汪......<br>";

    }
} 
class cat implements animal{
    public function run(){
        echo"小猫在奔跑<br>";

    }
    public function shout();{
        echo "喵喵......<br>";

    }
   

}
$dog=new dog();
$dog->run();
$dog->shout();
$cat=new cat();
$cat->run();
$cat->shout9;
?>
<?php
header("Content-Type:text/html;charset=utf-8");
interface animal{
    function run();
    function shout();
}
interface landanimal{
    public function liveonland();

}
class dog implements animal, landanimal{
    public function run(){
        echo "小狗在奔跑<br>";

    }
    public function shout(){
        echo "汪汪......<br>";
    }
    public function liveonland(){
        echo"小狗在陆地上生活<br>";

    }
} 
$dog=new dog();
$dog->run();
$dog->shout();
$dog->liveonland();
?>
4.7多态
<?php
header("Content-Type:text/html;charset=utf-8");
abstract class animal{
    public function shout(){
        echo "汪汪......<br>";

    }

}
class cat extends animal{
    public function shout(){
        echo "喵喵......<br>";

    }
}
function animalshout($obj);{
    if($obj instanceof  animal){
        $obj->shout();

    }else{
        echo "Error;对象错误！"
    }
}