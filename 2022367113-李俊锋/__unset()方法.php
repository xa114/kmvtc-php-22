<?
class animal {  
     private $name;  //私有的成员属性
     private $color;
     private $age;
     //__get（）方法用来获取私有属性
     function __get( $property_name) {
       if (isset( $this-> $property_name)) {
            return ( $this-> $property_name);
        } else {
            return(NULL);
        }
     }
     //__set()方法用来设置私有属性
     function __set( $property_name, $value) {
         $this->$property_name=$value;
     }
     //_isset（）方法
     function __isset( $property_name) {
        return isset( $this->$property_name);
     }
     //__unset（）方法
     function __unset( $property_name) {
        unset ( $this->$property_name);
     } 
    }
    $pig=new animal();
    $pig->name="小猪";
    echo var_dump(isset( $pig->name)) . "<br>";
    //调用__isset（）方法，输出：bool（true）
    echo $pig->name . "<br>";
    unset( $pig->name);//调用__unset()方法
    echo $pig->name;//无输出  
?>