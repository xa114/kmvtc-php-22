?php
//定义animal接口
interface animal{
    function run( ) ;
    function shout( ) ;
}
//定义 landanimal接口
interface landanimal{
    public function liveonland( );
}
//定义 dog类，实现 animala接口和landanimal接口
class dog implements animal,  landanimal{
    //实现接口中的抽象方法
    public function run( ) {
        echo "小狗在奔跑<br>";
    }
    public function shout( ) {
        echo "汪汪... ...<br>";
    }
    public function liveonland( ) {
        echo "小狗在陆地上生活<br>";
    }
}
$dog=new dog( );
$dog->run( );
$dog->shout( );
$dog->liveonland( );
?>