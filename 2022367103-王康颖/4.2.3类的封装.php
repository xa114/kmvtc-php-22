<?php
class MyClass 
{
    public    $public    = 'Public';
    //第一个public是访问修饰符；
    //第二个public是一个叫做public的成员属性，变量
    //第三个public是$public的成员属性的值
    protected $protected = 'Protected';
    private   $private   = 'Private';
   


    function printHello()  {
        // 成员内部引用成员属性，无任何权限问题
        echo $this->$public ;
        echo $this->$private ;
        echo $this->$protected ;
    }
}

$obj = new MyClass();                         //实例化当前类
        echo $obj->printHello();              //输出$public
//echo $obj->protected; //Fatal error:Cannot access protected property MyClass::$protected
//echo $obj->private; //Fatal error:Cannot access private property MyClass::$private

$obj->printHello();
/**
 * 定义类 MyClass2
 */

//子类中的访问修饰符========================
class Myclass2 extends MyClass {
    //可以访问公共属性和保护属性，但是私有属性不可以访问,成员内部引用成员属性，无任何权限问题
    protected $protected = 'Protected2';
    function printHello(){
        echo $this->public;
        echo $this->protected; // 不显示
        echo $this->private;
    }
}




$obj2 = new MyClass2();
echo $obj2->public;
//echo $obj2->protected;  //Cannot access protected property MyClass2::$protected
echo $obj2->private;      //未定义
echo $obj2->printHello(); //只显示公共属性和保护属性，不显示私有属性

?>