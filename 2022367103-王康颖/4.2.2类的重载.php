<?php
    class rz {

// ====================成员属性========================
    public $name;
    public $zhongzu;
    public $age;


// 构造函数 (创建对象时为对象赋初始值)
// parent::__construct($name, $zhongzu, $age)
public function __construct($name, $zhongzu, $age) {
    $this->name  = $name;
    $this->zhongzu = $zhongzu;
    $this->age   = $age;
}

// ====================成员方法========================
    public function getInfo() {
        echo "忍者的名称:". $this->name  . "<br>";
        echo "忍者的种族:". $this->zhongzu . "<br>";
        echo "忍者的年龄:". $this->age   . "<br>";
    }


}


    // 类的继承，新建一个[kkx]子类继承忍者类
    class kkx extends rz{
    // 成员属性：眼睛
        public $yj;
    // 重载：继承类的方法的继承写法[parent::]
        public function getInfo(){
    // 使用[parent::]来获取父类的成员方法或者属性
        parent::getInfo();
    // 书写kkx特有的属性，信息
        echo "卡卡西有" . $this->yj . '脸<br>';
        $this->xly();
    }

    // 子类[kkx]具有的成员方法
    public function xly(){
        echo  "我有写轮眼！！！";
    }

    // 继承类的构造函数的继承写法[parent::]
    public function __construct($name, $zhongzu, $age, $yj) {
        parent::__construct($name, $zhongzu, $age);
        // 卡卡西特有的眼睛属性需要单独书写
        $this->yj = $yj;
    }
}



    $crow = new kkx("旗木卡卡西","旗木","28","帅气的");
    $crow->getInfo();

?>