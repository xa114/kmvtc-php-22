<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <meta charset="UTF-8">
    <title>九九乘法表</title>
</head>
<body>
    <table border="1">
        <?php
        // 外层循环控制行数，即乘数
        for ($i = 1; $i <= 9; $i++) {
            echo "<tr>"; // 开始新的一行
            // 内层循环控制列数，即被乘数
            for ($j = 1; $j <= $i; $j++) {
                // 计算当前单元格的乘法结果
                $result = $i * $j;
                // 输出乘法结果，并在其后加上相应的中文注释
                echo "<td>$i × $j = $result</td>";
            }
            echo "</tr>"; // 结束当前行
        }
        ?>
    </table>
</body>
</html>