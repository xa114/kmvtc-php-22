<?php
class animal {
    public $name; //成员属性
    public $color;
    public $age;
    public function __construct( $name, $color, $age) {
        //构造
        $this->name= $name;
        $this->color= $color;
        $this->age= $age;
    }
    public function getInfo() {
        echo "动物的名称:" .  $this->name . "<br>" ;
        echo "动物的颜色：" . $this->color . "<br>" ;
        echo "动物的年龄：" . $this->age . "<br>" ;
    }
    /* *
      * 析构方法，在对象销毁时自动调用
      */
     public function __destruct() {
        echo "哈喽：". $this->name."<br>";
    }
}
$dog=new animal('小狗','白色',1);
$dog->getInfo();
$cat=new animal('小猫','橘色',2);
$cat->getInfo();
?>