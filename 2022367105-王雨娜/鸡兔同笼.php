<!DOCTYPE html>
<html>
<head>
    <title>鸡兔同笼计算器</title>
</head>
<body>
    <h2>鸡兔同笼计算器</h2>

    <form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
        <p>总共有多少个头？<input type="number" name="heads" required></p >
        <p>总共有多少只脚？<input type="number" name="feet" required></p >
        <input type="submit" name="calculate" value="计算">
    </form>

    <?php
    if (isset($_POST['calculate'])) {
        $heads = $_POST['heads'];
        $feet = $_POST['feet'];

        // 计算鸡和兔子的数量
        $rabbits = ($feet - 2 * $heads) / 2;
        $chickens = $heads - $rabbits;

        // 检查解是否合法
        if ($rabbits >= 0 && $chickens >= 0 && $chickens == intval($chickens) && $rabbits == intval($rabbits)) {
            echo "<h3>鸡的数量：" . $chickens . "</h3>";
            echo "<h3>兔子的数量：" . $rabbits . "</h3>";
        } else {
            echo "<h3>无法找到合法的鸡和兔子数量。</h3>";
        }
    }
    ?>
</body>
</html>