<?php
class animal {
    private $name; //私有的成员属性
    private $color;
    private $age;
    //_get（）方法来获取私有属性
    function __get( $property_name) {
        if (isset( $this-> $property_name)) {
            return ( $this-> $property_name);
        } else {
            return(NULL);
        }

    }
    //_get()方法用来设置私有属性
    function __set( $property_name, $value) {
        $this-> $property_name = $value;
    }
    //_isset()方法
    function __isset( $property_name) {
        return isset( $this-> $property_name) ;
    }

    //_unest()方法
    function __unset( $property_name) {
        unset( $this-> $property_name) ;
    }
}
$dog=new animal();
$dog->name="小狗" ;
echo var_dump(isset( $dog->name)) . "<br>" ;
//调用__isset()方法，输出：bool(true)
echo $dog->name . "<br>" ;   //输出：小猪
unset( $dog->name) ; //调用__unset()方法
echo $dog->name; //无输出
?>