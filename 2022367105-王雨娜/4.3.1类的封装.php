<?php
/* *
*定义类MyClass
*/
class MyClass {
    public $public='Public';
    //第一个是访问修饰符，第二个是成员属性，变量；第三个是前一个的成员属性的值
    protected $protected='Protected';
    private $private='Private';
    function printHello() {
        echo $this->public;
        echo $this->protected;
        echo $this->private;
    }
}
$obj =new MyClass();
echo $obj->public;
//echo $obj->protected; //Fatal error: Cannot assess protected property MyClass:: $prootected
//echo $obj->private; //Fatal error: Cannot assess private property MyClass:: $private $obj->printHello();
$obj->printHello();
/* *
* 定义类 MyClass2
*/
class MyClass2 extends MyClass {
    //可以访问公共属性和保护属性，但是私有属性不可以访问
    protected $protected='Protected2';
    function printHello() {
        echo $this->public;
        echo $this->protected;
        echo $this->private;
    }
}
$obj2=new MyClass2();
echo $obj2->public;
//echo $obj2->protected;
echo $obj2->private ;
$obj2->printHello();
?>