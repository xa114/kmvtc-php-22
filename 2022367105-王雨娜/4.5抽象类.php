<?php
header("Content-Type:text/html;charset=utf-8");
abstract class animal{
    abstract public function shout();
}
    class duck extends animal{
        public function shout() {
            echo "嘎嘎......<br>";
        }
}
class cat extends animal{
    public function shout() {
        echo "喵喵......<br>";
    }
}
$duck=new duck();
$duck->shout();
$cat=new cat();
$cat->shout();
?>