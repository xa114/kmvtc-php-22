<?php
class animal{
    private $name;
    private $color;
    private $age;
    private static $sex ="雌性" ; //私有的静态属性
    public function __construct( $name, $color, $age) {
        $this->name= $name;
        $this->color= $color;
        $this->age= $age;
    }
    public function getInfo() {
        echo "动物的名称:" . $this->name . "<br>" ;
        echo "动物的颜色:" . $this->color . "<br>" ;
        echo "动物的名称:" . $this->age . "<br>" ;
        //echo "动物的性别:".self:: $sex;
        self::getSex() ;
    }
    private static function getSex() {
        echo "动物的性别：" . self:: $sex;
    }
}
$cow=new animal("小奶牛","花色",2);
$cow->getInfo() ;
?>