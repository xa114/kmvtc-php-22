<?php
class fruit {
// ====================成员属性========================
     private $name;
     private $color;
     private $prize;

// ====================成员属性========================
     public function getInfo() {
     echo "水果的名称:". $this->name  . "<br>";
     echo "水果的颜色:". $this->color . "<br>";
     echo "水果的价格:". $this->prize . "<br>";
}
// 构造函数 
private function __construct( $name, $color, $prize) {
    $this->name  = $name;
    $this->color = $color;
    $this->prize  = $prize;
}
//4.3.2.1 ——get()方法取值
     function __get($property_name){
    // 检查属性 this-> 当前类  的 property_name 属性 是否存在
       if(isset($this->$property_name)){
        return (this->$property_name);
    } else {
        return(NULL);
    }
}
//4.3.2.2 set  (赋值)
    function  __set($property_name,$value){
         $this->$property_name = $value;
}
//4.3.2.3 isset  (检查)
    function __isset($property_name) {
         return isset($this->$property_name);
}

//4.3.2.4 unset  (删除)
    function __unset($property_name) {
        unset($this->$property_name);
    }
}
$apple = new fruit("苹果","红色","12");
$apple->name="苹果";
echo var_dump(isset($apple->name)) . "<br>"; //调用isset方法，输出：
//4.3.2.4 unset 测试用例
echo $apple->name . "<br>";  // "苹果";
unset($apple->name);      // 调用__unset()方法
echo $apple->name;     // 空输出