<?php
header("Content-Type:text/html;charset=utf-8");
//使用abstract关键字声明一个抽象类
abstract class animal{
    //在抽象类中声明抽象方法
    abstract public function shout();
}
//定义dog类继承animal类
class dog extends animal{
    //实现抽象方法
    public function shout(){
        echo "汪汪……<br>";
    }
}
//定义cat类继承animal类
class cat extends animal{
    //实现抽象方法
    public function shout(){
        echo "喵喵……<br>";
    }
}
$dog=new dog();
$dog->shout();
$cat=new cat();
$cat->shout();
?>


4.6 接口
<?php
header ("Content-Type:text/html;charset=utf-8");
//定义接口
interface animal{
    //声明抽象方法
    function run ();
    function shout();
}
//定义dog类，实现animal接口
class dog implements animal{
    //实现接口中的抽象方法
    public function run (){
        echo "小狗在奔跑<br>";
    }
    public function shout(){
        echo "汪汪……<br>";
    }
}
//定义cat类，实现animal接口
class cat implements animal{
    //实现接口中的方法
    public function run(){
        echo "小猫在奔跑<br>";
    }
    public function shout(){
        echo "喵喵……<br>";
    }
}
$dog=new dog();
$dog->run();
$dog->shout();
$cat=new cat();
$cat->run();
$cat->shout();
?>

<?php
header ("Content-Type:text/html;charset=utf-8");
//定义animal接口
interface animal{
    function run ();
    function shout();
}
//定义landanimal接口
interface landanimal{
    public function liveonland();
}
//定义dog类，实现animal接口和landanimal接口
class dog implements animal, landanimal{
    //实现接口中的抽象方法
    public function run (){
        echo "小狗在奔跑<br>";
    }
    public function shout(){
        echo "汪汪……<br>";
    }
    public function liveonland(){
        echo "小狗在陆地上奔跑<br>";
    }
}
$dog=new dog();
$dog->run();
$dog->shout();
$dog->liveonland();
?>




4.7多态
<?php
header ("Content-Type:text/html;charset=utf-8");
abstract class animal {
    public abstract function shout();
}
//定义dog类，实现抽象类中的方法
class dog extends animal {
    public function shout(){
        echo "汪汪……<br>";
    }
}
//定义cat类，实现抽象类中的方法
class cat extends animal{
    public function shout(){
        echo "喵喵……<br>";
    }
}
function animalshout( $obj){
    if( $obj instanceof animal){
        $obj->shout();
    } else{
        echo "Error:对象错误！"；
    }
}
$cat=new cat();
$dog=new dog();
animalshout( $cat);
?>