<?php
header("Content-Type:text/html;charset=utf-8");
calss cell phone {
    private $brand;
    private $size;
    private $price;
    public function __construct( $brand, $size, $price) {
        $this->brand= $brand;
        $this->size= $size;
        $this->price= $price;
    }
    public function getInfo() {
         echo "手机品牌:" . $this->brand . "<br>";
         echo "手机尺寸:" . $this->size . "<br>";
         echo "手机价格:" . $this->price . "<br>";
    }
}
$iPhone14=new cell phone ("苹果","6.1英寸","3899");
echo $iPhone14->brand;
?>

<?php
/* *
* 定义类 MyClass
*/
class MyClass {
   public $public='Public';
   protected $protected='Protected';
   private $private='Private';
   function printHello() {
    echo $this->public;
    echo $this->protected;
    echo $this->private;
   }
}
$obj=new MyClass();
echo $obj->public;
//echo $obj->protected; //Fatal error: Cannot access protected property MyClass:: $protected
//echo $obj->private; //Fatal error: Cannot access private property MyClass:: $private
$obj->printHello();
/* *
* 定义类 MyClass2
*/
class MyClass2 extends MyClass {     //继承MyClass类
        //可以访问公共属性和保护属性，但私有属性不可以访问
    protected $protected='Protected2';
    function printHello() {
        echo $this->public;
        echo $this->protected;
        echo $this->private;
    }
}
$obj2=new MyClass2();
echo $obj2->public;
//echo $obj2->protected;
echo $obj2->private;
$obj2->printHello();
?>