<?php
header("Content-Type:text/html;charset=utf-8");
class animal {
   private $name;
   private $color;
   private $age;
   //_get()方法用来获取私有属性的值
   public function _get( $property_name){
    if (isset( $this->$property_name)) {
        return $this->$property_name;
    } else {
        return(NULL);
    }
   }
public function _set( $property_name, $value) {
    $this->$property_name= $value;
  }
}
$dog=new animal();
$dog->name="小狗";
$dog->color="白色";
$dog->age=4;
echo $dog->name."<br>";
echo $dog->color."<br>";
echo $dog->age."<br>";
?>
