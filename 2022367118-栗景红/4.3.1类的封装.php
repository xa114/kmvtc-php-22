<?php
/* *
* 定义类 MyClass
*/
class MyClass {
   public $public='Public';
   protected $protected='Protected';
   private $private='Private';
   function printHello() {
    echo $this->public;
    echo $this->protected;
    echo $this->private;
   }
}
$obj=new MyClass();
echo $obj->public;
//echo $obj->protected; //Fatal error: Cannot access protected property MyClass:: $protected
//echo $obj->private; //Fatal error: Cannot access private property MyClass:: $private
$obj->printHello();
/* *
* 定义类 MyClass2
*/
class MyClass2 extends MyClass {     //继承MyClass类
        //可以访问公共属性和保护属性，但私有属性不可以访问
    protected $protected='Protected2';
    function printHello() {
        echo $this->public;
        echo $this->protected;
        echo $this->private;
    }
}
$obj2=new MyClass2();
echo $obj2->public;
//echo $obj2->protected;
echo $obj2->private;
$obj2->printHello();
?>