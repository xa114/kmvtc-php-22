<?php
header("Content-Type:text/html;charset=utf-8");
//使用abstract关键字声明一个抽象类
abstract class anima {
         //在抽象类中声明抽象方法
         abstract public function shout();
}
//定义dog类继承animal类
class dog extends animal{
     //实现抽象方法
     public function shout() {
           echo "汪汪......<br>";
     }
}
//实现cat类定义继承animal类
class cat extends animal{
     //实现抽方法
     public function shout() {
        echo "喵喵......<br>";
     }
}

//实例化dog类
$dog=new dog();
//调用dog类的shout方法
$dog->shout();
//实例化cat类
$cat=new cat();
//调用cat类的shout方法
$cat->shout();
?>