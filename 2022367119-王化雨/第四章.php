<?php
class fruit {
    public $name;
    public $color;
    public $taste;
public function __construct ( $name, $color, $taste ) {
    //echo "我是构造方法";
    $this->name= $name;
    $this->color= $color;
    $this->taste= $taste;
}
public function getImfo() {
    echo "水果的名称：" . $this->name . "<br>";
    echo "水果的颜色：" . $this->color . "<br>";
    echo "水果的味道：" . $this->taste . "<br>";
}
}
$apple = new fruit("桃子","红色","甜");
$apple->getImfo();
$plum = new fruit("番石榴","绿色","酸甜");
$plum->getImfo();
$litchi = new fruit("芒果","黄色","甜");
$litchi->getImfo();
?>