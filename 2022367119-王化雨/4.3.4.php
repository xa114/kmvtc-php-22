<?php
header("Content-Type:text/html;charset=utf-8");
class drink {
    private $name;//私有成员属性
    private $color;
    private $tea;
    //_get（）方法用来获取私有属性的值
    public function _($propety_name) {
        if (isset($this->$propety_name)) {
            return $this->$propety_name;
        } else {
            return(NULL);
        }
    }
    //_se()方法用来设置私有属性的值
    public function _($propety_name, $value) {
        $this->$propety_name=$value;
    }
//_isset()方法
function_isset($propety_name){
    unset($this->$propety_name);
}}
$tea = new drink();
$tea->name="奶茶";
echo var_dump(isset($tea->name)) . "<br>";
//调用_isset()方法，输出：bool(true)
echo $tea->name."br"; //输出:奶茶
unset($tea->name);//调用_unset()方法
echo $tea->name;//无输出
?>