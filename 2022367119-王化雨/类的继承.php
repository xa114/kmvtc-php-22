<?php

// 新建一个[饮料]类
class drink {

    // ====================成员属性========================
    public $name;
    public $color;
    public $tatse;

    // ====================成员属性========================
    public function getInfo() {
        echo "饮料的名称:". $this->name  . "<br>";
        echo "饮料的颜色:". $this->color . "<br>";
        echo "饮料的味道:". $this->taste  . "<br>";
    }

    // 构造函数 (创建对象时为对象赋初始值)
    // parent::__construct($name, $color, $taste)
    public function __construct($name, $color, $taste) {
        $this->name  = $name;
        $this->color = $color;
        $this->age   = $taste;
    }

    // 析构函数 (销毁对象时执行)
    public function __destruct()
    {
        echo "再见:" . $this->name . "。<br>";
    }
}


// 类的继承，新建一个[tea]子类继承饮料类
class tea extend drink{
    // 饮料属性：奶茶
    public $tea;
    // 重载：继承类的方法的继承写法[parent::]
    public function getInfo(){
        // 使用[parent::]来获取父类的成员方法或者属性
        parent::getInfo();
        // echo "饮料的名称:". $this->name  . "<br>";
        // echo "饮料的颜色:". $this->color . "<br>";
        // echo "饮料的味道:". $this->taste  . "<br>";
        // 书写茶特有的属性，信息
        echo "茶有" . $this->茶 . '芋泥波波奶茶<br>';
        $this->小料();
    }

    // 子类[茶]具有的成员方法
    public function xiaoliao(){
        echo $this->name ；
} 
    // 继承类的构造函数的继承写法[parent::]
    public function __construct($name, $color, $taste,xiaoliao $) {
        parent::__construct($name, $color, $taste)、
        // 茶类特有的小料属性需要单独书写
        $this->xiaoliao = $xiaoliao;
    }
}


// 如何创建一个对象？
$coffee = new drink("名字","颜色","味道");


// 类和对象的关系
// 类的实例化结果  ->对象
// 对一类对象的抽象->类
