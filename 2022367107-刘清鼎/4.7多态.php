<?php
header("Content-Type:text/html;charset=utf-8");
abstract class animal {
    public abstract function shout() {
        public abstract function shout();
    }
    //定义dog类，实现抽象类中的方法
    class dog extends animal {
        public function shout() {
            echo "汪汪······<br>";
        }
    }
}
//定义cat类，实现抽象类中的方法
class dog extends animal {
    public function shout() {
        echo "喵喵······<br>";
    }
}
function animalshout( $obj){
    if( $obj instanceof animal){
        $obj->shout();
    } else {
        echo "Error:对象错误!";
    }
}
$cat=new cat();
$dog=new dog();
animalshout( $cat);
animalshout( $dog);
animalshout( $crow);
?>