<?php
class animal {
   private $name;
   private $color;
   private $age;
   private static $sex="雄性";   //私有的静态属性
   //静态成员方法
   public static function getInfo() {
      echo "动物是雄性的。";
   }
}
//echo animal::$sex;    //错误，静态属性性私有，外部不能访问
animal::getInfo();      //调用静态方法
?>

<?php
header("Content-Type:text/html;charset=utf-8");
class animal {
   private $name;
   private $color;
   private $age;
   private static $sex="雄性";  //私有的静态属性
   public function _consruct( $name, $color, $age) {
      $this->name= $name;
      $this->color= $color;
      $this->age= $age;
   }
   public function getInfo() {
     echo "动物的名称:" . $this->name . "<br>";
     echo "动物的颜色:" . $this->color . "<br>";
     echo "动物的年龄:" . $this->age . "<br>";
        //echo "动物的性别:".self::$sex;
      self::getSex();
   }
   private static function getSex() {
      echo "动物的性别:" . self::$sex;
   }
}
$dog=new animal("小狗","黑色",4);
$dog->getInfo();
?>