<?php

// 新建一个[花]类
class car {

    // ====================成员属性========================
    public $brand;
    public $drive;
    public $colour;
    public $price;

    // ====================成员属性========================
    public function getInfo() {
        echo "花的品牌:" . $this->brand . "<br>" ;
        echo "花的种类:" . $this->drive . "<br>" ;
        echo "花的颜色:" . $this->colour . "<br>" ;
        echo "花的价格:" . $this->price . "<br>" ;
    }

    // 构造函数 (创建对象时为对象赋初始值)
    // parent::__construct($brand,$drive,$colour,$price)
    public function __construct($brand,$drive,$colour,$price) {
        $this->brand = $brand;
        $this->drive = $drive;
        $this->colour = $colour;
        $this->price = $price;
    }

    // 析构函数 (销毁对象时执行)
    public function __destruct()
    {
        echo "再见:" . $this->brand . "。<br>";
    }
}


// 类的继承，新建一个[supercar]子类继承动物类
class supercar extend car{
    // 成员属性：尾翼
    public $empennage;
    // 重载：继承类的方法的继承写法[parent::]
    public function getInfo(){
        // 使用[parent::]来获取父类的成员方法或者属性
        parent::getInfo();
        // echo "花的品牌:". $this->brand  . "<br>";
        // echo "花的种类:". $this->drive . "<br>";
        // echo "花的颜色:". $this->color   . "<br>";
        // echo "花的价格:". $this->price   . "<br>";
        // 书写跑车类特有的属性，信息
        echo "花类有" . $this->empennage . '颜色<br>';
        $this->drifting();
    }

    // 子类[supercar]具有的成员方法
    public function drifting(){
        echo $this->brand . "is driftinging...";
    }
 
    // 继承类的构造函数的继承写法[parent::]
    public function __construct($brand,$drive,$colour,$price,$empennage) {
        parent::__construct($brand,$drive,$colour,$price)、
        // 跑车类特有的尾翼属性需要单独书写
        $this->empennage = $empennage;
    }
}


// 如何创建一个对象？
$GTR = new CAR("品牌","种类","颜色","价格");


// 类和对象的关系
// 类的实例化结果  ->对象
// 对一类对象的抽象->类
