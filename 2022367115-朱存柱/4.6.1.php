<?php
header("content-type:text/html;charset=utf-8");
interface animal{
    function run();
    function shout();
}
interface landanimal{
    public function liveonland();
}
class dog implements animal, landanimal{
    public function run();
echo "小狗在奔跑<br>";
}
public function shout() {
    echo "玩玩......<br>";
}
class cat implements animal, landanimal{
    public function run();
echo "小猫在奔跑<br>";
}
public function shout() {
    echo "兔兔......<br>";
}
$dog=new dog();
$dog->run();
$dog->shout();
$cat=new cat();
$cat->run();
$cat->shout();