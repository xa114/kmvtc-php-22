<?php
class animal {
    private $name;
    private $color;
    private $age;
    public function __construct( $name, $color, $age) {
        $this->name=$name;
        $this->color=$color;
        $this->age=$age;
    }
    public function getinfo() {
        echo'名字:' . $this->name . ,'颜色:' . $this->color . ,'年龄:' . $this->age . '.<br>';
    }
    public function __clone() {
        $this->name="狗";
        $this->color="黑";
        $this->age="200岁";
    }
}
$pig=new animal('野牛','黑校色','1000岁');
$pig->getinfo();
$pig2=clone $pig;
$pig2->getinfo();
?>