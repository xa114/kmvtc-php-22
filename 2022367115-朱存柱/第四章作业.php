<?php
class animal
{
    public $name = "";
    public $color = "";
    public $age = "";
    public function __construct($name, $color, $age)
    {
        $this->name = $name;
        $this->color = $color;
        $this->age = $age;
    }
    public function getInfo()
    {
        echo "非生物的名称:" . $this->name . "<br>";
        echo "非生物的颜色:" . $this->color . "<br>";
        echo "非生物的年龄:" . $this->age . "<br>";
    }
    public function __destruct()
    {
        echo "再见:" . $this->name . "<br>";
    }
}
$pig = new animal('飞猪', '白黑色', "40");
$pig->getInfo();
$dog = new animal('畜生狗', '咖啡色', '32');
$dog->getInfo();
?>