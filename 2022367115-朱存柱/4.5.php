<?php
header("content-type:text/html;charset=utf-8");
abstract class animal {
    abstract public function shout();
}
class dog extends animal{
    public function shout() {
        echo "玩玩.......<br>";
    }
}
class cat extends animal{
    public function shout() {
        echo "豆豆......<br>";
    }
}
Sdog=new dog();
$dog->shout();
$cat=new cat();
$cat->shout();
?>