<?php
phpinfo();
?>
<!--标椎标记-->
<?php
echo'Hello World!'
?> 
<!--简短标记-->
<?php
echo'hello world!'

?>
<!--单行注释-->
<?php
echo'hello world!';

?>
<!--多行注释-->
<?php
/*
echo 'hello world!';
echo'hi php';
*/
?>

<?php
{
    echo'hello world!';
    echo'<br>';
    echo'hi php';
}
?>
<!--攻击-->
<?php
class hero
{
    //血量
    public $hp = 1000;
    //姓名
    public $name;
    //攻击力
    public $attack;

    public $shield = 0.2;

    function __construct($name, $attack)
    {
        $this->name = $name;
        $this->attack = $attack;
    }

    // 攻击
    function attack($hero)
    {
        echo $this->name, "攻击了", $hero->name,"<br>";
        $hero->hp -= $this->attack;

        $hero->hp = $hero->hp - $this->attack*(1-$hero->shield);
        echo $hero->name,"的剩余血量：", $hero->hp,"<br>";
    }
}

$hero1 = new hero("王康颖",60);
$hero2 = new hero("张盛杰",90);

$hero1->attack($hero2);
$hero2->attack($hero1);
$hero1->attack($hero2);
$hero2->attack($hero1);
$hero1->attack($hero2);
$hero2->attack($hero1);
$hero1->attack($hero2);
$hero2->attack($hero1);
$hero1->attack($hero2);
$hero2->attack($hero1);
$hero1->attack($hero2);
$hero2->attack($hero1);
$hero1->attack($hero2);
$hero2->attack($hero1);
$hero1->attack($hero2);
$hero2->attack($hero1);
$hero1->attack($hero2);
$hero2->attack($hero1);
$hero1->attack($hero2);
$hero2->attack($hero1);
$hero1->attack($hero2);
$hero2->attack($hero1);
$hero1->attack($hero2);
$hero2->attack($hero1);
$hero1->attack($hero2);
$hero2->attack($hero1);
$hero1->attack($hero2);
$hero2->attack($hero1);
$hero1->attack($hero2);
$hero2->attack($hero1);
$hero1->attack($hero2);
$hero2->attack($hero1);





?>

<!--鸡兔同笼-->
<!DOCTYPE html>
<html>
<head>
    <title>鸡兔同笼</title>
    <meta charset="UTF-8">
</head>
<body>
    <table align="center" border="1" width="500">
        <caption><h1>计算器</h1></caption>
        <form action="">
        <tr>
                <td>
                    <div>头</div>
                </td>

                <td>
                    <div>脚</div>
                </td>
                <td><div>按钮</div></td>
            </tr>
            <tr>
                <td>
                    <input type="text" size="5" name="num1" value="<?php echo $_GET["num1"] ?? ''; ?>">
                </td>
                <td>
                    <input type="text" size="5" name="num2" value="<?php echo $_GET["num2"] ?? ''; ?>">
                </td>
                <td><input type="submit" name="sub" value="计算"></td>
            </tr>
            <?php if (isset($_GET["sub"])): ?>
            <tr><td colspan="4">
            <?php
                $num1 = $_GET["num1"] ?? null;
                //echo "已知头共有",$num1,"只,";
                $num2 = $_GET["num2"] ?? null;
                //echo "已知脚共有",$num2,"只,";

                if (is_numeric($num1) && is_numeric($num2)) {
                    $num1 = (float)$num1;
                    $num2 = (float)$num2;
                    $rabbit_num =($num2 - 2*$num1) /2;
                    $chick_num = $num1 - $rabbit_num;
                    
                    echo "鸡有" . $chick_num,",兔有" . $rabbit_num;
                } else {
                    echo $message;
                }
            ?>
            </td></tr>
            <?php endif; ?>
        </form>
    </table>
</body>
</html>
<?
/整型
$n1 = 123;
echo "n1=", $n1, "<br>";
$n2 = 0;
echo "n2=", $n2, "<br>";
$n3 = -123;
echo "n3=", $n3, "<br>";
$n4 = 0123;
echo "n4=", $n4, "<br>";
$n5 = 0xFF;
echo "n5=", $n5, "<br>";
$n6 = 0b11111111;
echo "n6=", $n6, "<br>";
?>

<?
    //浮点型
$pi = 3.1415926;
echo "pi=", $pi, "<br>";
$width = 3.3e4;
echo "width=", $width, "<br>";
$var = 3e-2;
echo "var=", $var, "<br>";
?>