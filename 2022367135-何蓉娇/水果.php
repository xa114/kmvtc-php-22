<?php
class fruit {
    public $name ;
    public $color ;
    public $price;
    public function __construct( $name, $color, $price) {
        $this->name= $name;
        $this->color= $color;
        $this->price= $price;
    }
    public function getInfo() {
        echo "水果的名称:" . $this->name . "<br>" ;
        echo "水果的颜色:" . $this->color . "<br>" ;
        echo "水果的价格:" . $this->price . "<br>" ;
    }
    public function __destruct() {
        echo "再见:" . $this->name."<br>" ;
    }
}
$apple=new fruit('苹果','红色',12);
$apple->getInfo() ;
$watermelon=new fruit('西瓜','绿色',5);
$watermelon->getInfo() ;
$strawberry=new fruit('草莓','红色',15);
$strawberry->getInfo() ;
?>