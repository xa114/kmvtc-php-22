<?php
header( "Content-Type:text/html;charset=utf-8" );
class animal {
    private $name;         //私有属性 name
    private $color;         //私有属性 color
    private $age;         //私有属性 age
    public function __construct( $name,$color,$age) {
        $this->name = $name;
        $this->color = $color;
        $this->age = $age;
    }
    public function getInfo( ) {
        echo "动物的名称：" . $this->name . "<br>";
        echo "动物的颜色：" . $this->color . "<br>";
        echo "动物的年龄：" . $this->age . "<br>";
    }
}
$dog =new animal("小狗","白色",5);
//$dog->getInfo();
echo $dog->name;
?>