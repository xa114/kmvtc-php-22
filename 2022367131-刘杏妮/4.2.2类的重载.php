<?php
class animal{
    public $name;
    public $color;
    public $age;
    public function _construct($name,$color,$age) {
        $this->name= $name;
        $this->color= $color;
        $this->age= $age;
    }
    public function getInfo() {
        echo "动物名称：" . $this->name ."<br>";
        echo"颜色:" . $this->color . "<br>";
        echo"年龄：" . "<br>";
    }
    //
    public function _destruct_destruct() {
        echo "再见:" . $this->name."<br>";
    }

}
//定义一个elephant类，使用extends关键字来继承animal类，作为animal类的子类
class elephant extends animal {//类的继承，新建一个[elephant]子类继承
    public $nose;//elephant类自有属性 成员属性：鼻子
    //重载:继承类的方法的继承写法[parent：；] 
    public function getInfo() {
        // 使用[parent：：]来获取父类的成员方法或者属性
        parent::getInfo();
        echo "大象有" . $this->nose . '鼻子<br>';
        $this->drink()
    }
    public function drink() {
        //象类自有的方法
        echo "我会用鼻子喝水！！！";
    }
    public function __construct( $name, $color, $age, $swing) {
        parent::__construct( $name, $color, $age);
        $this->nose= $nose;
    }   
}

$dog= new animal('狗','白色',3);
$dog->getInfo();
$elephant= new animal('大象','灰白色',10,"长长的");
$elephant->getInfo();
$peacock= new animal('孔雀','彩色',2);
$peacock->getInfo();
$lion= new animal('狮子','白色',5);
$lion->getInfo();
?>