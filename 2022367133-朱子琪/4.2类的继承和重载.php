<?php
//类的继承
//定义一个bird类，使用extends关键字来继承animal类，作为animal类的子类
class bird extends animal{
    public $wing;    //bord类的自有属性$wing
    public function fly(){
        //bird类自有的方法
        echo'I can fly!!!'
    }
}
$corw = new bird ("乌鸦","黑色",3)；
$corw ->getinfo();
$corw ->fly(;
)


// 类的重载，新建一个[bird]子类继承动物类
class bird extend animal{
    // 成员属性：翅膀
    public $wing;
    // 重载：继承类的方法的继承写法[parent::]
    public function getInfo(){
        // 使用[parent::]来获取父类的成员方法或者属性
        parent::getInfo();
        // echo "动物的名称:". $this->name  . "<br>";
        // echo "动物的颜色:". $this->color . "<br>";
        // echo "动物的年龄:". $this->age   . "<br>";
        // 书写鸟类特有的属性，信息
        echo "鸟类有" . $this->wing . '翅膀<br>';
        $this->fly();
    }

    // 子类[bird]具有的成员方法
    public function fly(){ //鸟类自有的方法
        echo $this->name . "is flying...";
    }
 
    // 继承类的构造函数的继承写法[parent::]
    public function __construct($name, $color, $age, $wing) {
        parent::__construct($name, $color, $age)、
        // 鸟类特有的翅膀属性需要单独书写
        $this->wing = $wing;
    }
}
$corw = new bird ("乌鸦","黑色","4","漂亮的")；
$corw ->getinfo();






// 如何创建一个对象？
$cat = new animal("名字","颜色","年龄");


// 类和对象的关系
// 类的实例化结果  ->对象
// 对一类对象的抽象->类


//类的重载
?>