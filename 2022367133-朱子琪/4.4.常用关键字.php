<?php


//4.4.1 static 静态的
// 新建一个[动物]类
class animal {

    // ====================成员属性========================
    private $name;
    private $color;
    private $age;
    private static $sex = "雄性"; 
    // ====================成员属性========================
    // public function getInfo() {
    //     echo "动物的名称:". $this->name  . "<br>";
    //     echo "动物的颜色:". $this->color . "<br>";
    //     echo "动物的年龄:". $this->age   . "<br>";
    // }

    // 构造函数 
    public function __construct($name, $color, $age) {
        $this->name  = $name;
        $this->color = $color;
        $this->age   = $age;
    }

    private function getInfo()  {
        echo "动物是雄性的";   
    }
}

echo animal::$sex;
animal::getInfo();
?>

<?php
class animal{
    final public function getInfo() {
    }
}
class bird extends animal{
    public function getInfo(){
    }
}
$crow=new bird();
$crow->getInfo();




?>php

//4.4.3 self 指向【类-class】的本身，不能指向任何实例化的对象。
class animal {
    private static $firstCry=0;//私有的静态变量
    private $lastCry;
    //构造方法
    function __construct() {
        $this->lastCry=++self:: $firstCry;
    }
    function printLastCry() {
        var_dump( $this->lastCry);
    }
}
$bird=new animal(); //实例化对象
$bird->printLastCry(); //输出：int（1）



//4.4.4 const常量（是定义常量的关键字）
//const 用于定义常量，常量不可改变
//常量的值在定义后不能被修改
//常量的值不需要$符号
//常量在定义后可以直接使用，不需要$符号
class MyClass {
    //定义一个常量
    const constant='我是个常量！'；
    function showConstant() {
        echo self::constant."<br>"; //类中访问常量
    }
}
echo MyClass::constant."<br>";  //使用类名来访问常量
$class=new MyClass();
$class->showConstant();


//4.4.5__toString()方法
class TestClass{
    public $foo;
    public function __construct( $foo) {
        $this->foo= $foo;
    }

//定义一个__toString()方法，返回一个成员属性 $foo
    public function __toString() {
        return $this->foo;
    }
}
$class-new TestClass('hello');
//直接输出对象
echo $class;  //输出结果为hello

?>
//4.4.6__clone()方法
<?php
class animal{
    private $name;  //私有成员属性
    private $color;
    private $age;

    public function __construct( $name, $color, $age) {
         $this->name. "<br>";
         $this->color. "<br>";
         $this->age. "<br>";
        }
    public function getInfo() {
        echo"动物名称:" . $this->name. "<br>";
        echo"动物颜色:" . $this->color. "<br>";
        echo"动物年龄:" . $this->age. "<br>";
        }
    public function __clone() {
        $this->name ="狗"；
        $this->color ="黑"；
        $this->age ="2岁"；
        }
    }
    
    $pig = new animal("猪","粉色",2);
    $pig->getInfo();
    $pig2 = clone $pig；//克隆对象
    $pig2 ->getInfo();


?>
<?php
class Test{
    function __call( $function_name, $args){
        print"你所调用的函数：$function_name(参数:";
        print_r($args);
        print")不存在 ! <br>";
    }
}
$test=new Test();
$test->domo("one","two","three");


?>




