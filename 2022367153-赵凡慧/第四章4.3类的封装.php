<?php
class animal {
    //成员属性
    private $name;//私有属性
    private $color;
    private $age;
    //构造函数（创造对象时为成员属性赋初值，完成初始化工作）
public function __construct ( $name, $color, $age ) {
    //echo "我是构造方法";
    $this->name= $name;
    $this->color= $color;
    $this->age= $age;
}
//析构函数（在对象销毁时自动调用）
public function getImfo() {
    echo "动物的名称：" . $this->name . "<br>";
    echo "动物的颜色：" . $this->color . "<br>";
    echo "动物的年龄：" . $this->age . "<br>";
}

}
$cat= new animal("小猫","白色",5);
//$cat->getInfo();
echo $cat->name;
?>



<?php
class MyClass {
    public $publc='Public'; //定义公共属性$public    【公有的  无访问限制，不做特别说明，均默认为声明的是public，成员内部、外部均可访问】
    //第一个public是访问修饰符；
    //第二个public是一个叫做public的成员属性，变量；
    //第三个public是$public的成员属性的值；
    protected $protected='Protected'; //定义保护属性 $protected    【保护的  所属类及其子类享有访问权限，外部代码不可访问】
    private $private='Private'; //定义私有属性$private   【私有的  仅针对当前class享有访问权限，当前类的子类以及类的外部均不允许访问】
    function printHello() { //输出3个成员属性
        //成员内部引用成员属性，无任何权限问题
        echo $this->public;
        echo $this->protected;
        echo $this->private;
    }
}
$obj=new MyClass(); //实例化当前类
echo $obj->public; //输出$public
$obj->printhello();  //输出3个成员属性
/ * *
* 定义类MyClass2
*/
class MyClass2 extends MyClass {
    //继承Myclass类
    //可以访问公共属性和保护属性，但是私有属性不可以访问
    protected $protected='Protected2';
    function printHello() {
        echo $this->public;
        echo $this->protected;
        echo $this->private;
    }
}
$obj2=new MyClass2();
echo $obj2->public; //输出$public
echo $obj2->private; //未定义
$pbj2->printHello();//只显示公共属性和保护属性，不显示私有属性
?>



<?php
class animal {
    //成员属性
    private $name;//私有属性
    private $color;
    private $age;


//private[私有的]
//      * 1、仅针对当前class享有访问权限
//      * 2、当前类的子类不允许访问
//      * 3、类的外部均不允许访问
//      */



    //__grt()方法用来获取私有属性
    //4.3.2.2 get  (取值)
    function __get( $property_name) {
      // 检查属性 this-> 当前类  的 property_name 属性 是否存在
        if(isset($this-> $property_name)) {
            return ( $this-> $property_name);
        } else {
            return(NULL);
        }
    }
    //__set()方法用来设置私有属性
    //4.3.2.2 set  (赋值)
    public __set($property_name,$value){
        $this->$property_name = $value;
    }
    //4.3.2.3 isset  (检查)
    function __isset($property_name) {
        return isset($this->$property_name);
    }

    //4.3.2.4 unset  (删除)
    function __unset($property_name) {
        return unset($this->$property_name);
    }
}
$dog=new animal();
//__get __set 方法写了以后就会自动对private属性取值或者赋值
$dog->name="小狗";//自动调用__set()方法
$dog->color="白色"；
$dog->age=4;
echo $dog->name."<br>";//自动调用__get()方法
echo $dog->color."<br>";
echo $dog->age."<br>";


$pig=new animal();
$pig->name="小猪";
echo var_dump(isset( $pig->name)) . "<br>";
//调用__isset()方法，输出：bool（true)
echo $pig->name."<br>";    //输出：小猪
unset( $pig->name);//调用__unset()方法
echo $pig->name;//无输出
?>




