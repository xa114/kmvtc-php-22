<?php
class animal {
    //成员属性
    public $name;
    public $color;
    public $age;
    //构造函数（创造对象时为成员属性赋初值，完成初始化工作）
public function __construct ( $name, $color, $age ) {
    //echo "我是构造方法";
    $this->name= $name;
    $this->color= $color;
    $this->age= $age;
}
//析构函数（在对象销毁时自动调用）
public function getImfo() {
    echo "动物的名称：" . $this->name . "<br>";
    echo "动物的颜色：" . $this->color . "<br>";
    echo "动物的年龄：" . $this->age . "<br>";
}
//类的继承，新建一个[bird]子类继承动物类   定义一个bird类，使用extends关键字来继承animal类，作为animal类的子类
class bird extends animal {
    public $wing;//bird类的自有属性 $wing
    public function fly() {//bird类自有的方法
        echo 'I can fly!!!';
    }
}
}
//如何创建一个对象
$sparrow = new bird("麻雀","灰色","4");
$sparrow->getImfo();
$sparrow->fly();
?>