<?php
//gettype()
$a="Hello";
echo gettype($a)."<br>";
$b=array(1,2,5);
echo gettype( $b)."<br>";
//intval
echo intval(4.5)."<br>";
//var_dump
var_dump( $a);
echo "<br>";
var_dump( $a,$b);
?>

<?php
echo(floor(0.60));  //输出结果：0

echo(floor(0.40));  //输出结果：0

echo(floor(5));   //输出结果：5

echo(floor(5.1));  //输出结果：5

echo(floor(-5.1));  //输出结果： -6

echo(floor(-5.9));  //输出结果：-6
?>


<?php
echo rand();
echo rand();
echo rand(10,100);
?>


<?php
$pizza ="piece1 piece2 piece3 piece4 piece5 piece6";
$pieces = explode(" ", $pizza);
echo $pieces[0];  //输出piecel
echo $pieces[1];  //输出piece2
?>


<?php
echo md5("apple");
?>

<?php
var_dump(checkdate(12,31,2000));
var_dump(checkdate(2,29,2001));
?>


<?php
//设置时区
date_default_timezone_set("PRC");
var_dump(checkdate(12,31,2000))."<br>"; //输出:bool(true)
var_dump(checkdate(2,31,2000))."<br>"; //输出：bool(false)
//mktime()
echo time()."<br>";  //返回当前时间截

echo mktime(0,0,0,12,25,2016)."<br>";
//今天距离2030年国庆节还有的少天
echo "今天距2030年国庆节还有". ceil((mktime(0,0,0,10,1,2030)-time())/(24*60*60))."天<br>";
//date()返回格式化的当地时间
echo "现在是：".date('Y-m-d H:i:s');
?>
