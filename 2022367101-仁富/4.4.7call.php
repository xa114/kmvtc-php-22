<?php
// __call() 方法,当调用的方法不存在或者不可见时，__call() 方法会被调用
class callTest{
    //$name 参数是要调用的方法名称
    //$arguments 参数是一个数组，包含了要传递给方法的参数
    public function __call($function_name, $args) {
        print "你所调用的函数：$function_name(参数:" ;
        print_r($args);
        print")不存在! <br>";
    }

}
$test = new callTest();
$test->demo("one","two","three");
?>