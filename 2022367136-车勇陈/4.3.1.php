<?php

// 新建一个[动物]类
class animal {

    // ====================成员属性========================
    private $name;
    private $color;
    private $age;

    // ====================成员属性========================
    public function getInfo() {
        echo "动物的名称:". $this->name  . "<br>";
        echo "动物的颜色:". $this->color . "<br>";
        echo "动物的年龄:". $this->age   . "<br>";
    }

    // 构造函数 
    public function __construct($name, $color, $age) {
        $this->name  = $name;
        $this->color = $color;
        $this->age   = $age;
    }

}

$dog = new animal("鸡","颜色","年龄");

// 需要同学查看浏览器回显报错，下属代码在类的外部尝试获取animal类的实体化dog对象的name属性
echo $dog->name;

//==============================================================================================
// 访问修饰符
// 1.public    公有的  无访问限制，不做特别说明，均默认为声明的是public，成员内部、外部均可访问
// 2.private   私有的  仅针对当前class享有访问权限，当前类的子类以及类的外部均不允许访问
// 3.protected 保护的  所属类及其子类享有访问权限，外部代码不可访问

class MyClass 
{
    public    $public    = 'Public';
    //第一个public是访问修饰符；
    //第二个public是一个叫做public的成员属性，变量
    //第三个public是$public的成员属性的值
    private   $private   = 'Private';
    protected $protected = 'Protected';


    function printHello()  {
        // 成员内部引用成员属性，无任何权限问题
        echo $this->$public . "<br>";
        echo $this->$private . "<br>";
        echo $this->$protected . "<br>";
    }
}

$myClass = new MyClass();
$myClass->printHello();
/**
 * Public
 * Private
 * Protected
 */

// 在成员外部进行成员属性的直接访问，访问修饰符起到权限判断的作用
echo $myClass->public;
echo $myClass->private;
echo $myClass->protected;

/**
 * Public
 * Fatal error : ......(不能访问 private & protected 属性)
 * Fatal error : ......(不能访问 private & protected 属性)
 */

 //子类中的访问修饰符========================
class MyClass2 extend MyClass {
    protected $protected = 'Protected2';

    function printHello()  {
        // 成员内部引用成员属性，无任何权限问题
        echo $this->$public . "<br>";
        echo $this->$private . "<br>";    // 不显示
        echo $this->$protected . "<br>";
    }
}

$myClass2 = new MyClass2();
echo $myClass2->public;
echo $myClass2->private;
echo $myClass2->protected;

$myClass2->printHello();


// class Test432{
//     // private[私有的]仅针对当前class享有访问权限，当前类的子类以及类的外部均不允许访问
//     /**
//      * private[私有的]
//      * 1、仅针对当前class享有访问权限
//      * 2、当前类的子类不允许访问
//      * 3、类的外部均不允许访问
//      */
//     private $name;


//     public function __get($name){
//         // 检查属性 this-> 当前类  的 name 属性 是否存在
//         if(isset($this->$name)){
//             return (this->$name);
//         }else {
//             return(NULL);
//         }
//     }

//     public __set($name,$value){
//         $this->$name = $value;
//     }
// }

// $test432 = new  Test432();
// // 这样获取能否成功？
// echo $test432->name; // Fatal
// echo $test432->__get(name);
// $test432->name = "猪"; //fatal
// $test432->__set(name,"123");
// echo $test432->__get(name);


class animal {}

    // ====================成员属性========================
    private $name;
    private $color;
    private $age;

    // ====================成员属性========================
    public function getInfo() {
        echo "动物的名称:". $this->name  . "<br>";
        echo "动物的颜色:". $this->color . "<br>";
        echo "动物的年龄:". $this->age   . "<br>";
    }

    // 构造函数 
    public function __construct($name, $color, $age) {
        $this->name  = $name;
        $this->color = $color;
        $this->age   = $age;
    }

