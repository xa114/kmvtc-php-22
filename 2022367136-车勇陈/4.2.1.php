<?php
    // 新建一个[动物]类
    class animal {
    
        // ====================成员属性========================
        public $name;
        public $color;
        public $age;
    
            // 构造函数 (创建对象时为对象赋初始值)
        // parent::__construct($name, $color, $age)
        public function __construct($name, $color, $age) {
            $this->name  = $name;
            $this->color = $color;
            $this->age   = $age;
        }
        // ====================成员属性========================
        public function getInfo() {
            echo "动物的名称:". $this->name  . "<br>";
            echo "动物的颜色:". $this->color . "<br>";
            echo "动物的年龄:". $this->age   . "<br>";
        }
    
    }
    /**
    *定义一个bird类，使用 extends 关键字来继承 animal类，作为animal类的子类
    */
    class bird extends animal {
        public $wing;//bird类的自由属性 $wing
        public function fly() { //bird类自有的方法       
            echo 'I can fly!!!'
        }
    }
    $crow = new bird ( "小鸡","黑色",3);
    $crow->getInfo();
    $crow->fly();
    ?>