<?php
Class plants {
     public $name;
     public $color;
     public $age;
     public function __construct( $name, $color, $age) {
           $this->name= $name;
           $this->color= $color;
           $this->age= $age;
}
public function getInfo() {
      echo "植物的名称" . $this->name . "<br>";
      echo "植物的颜色" . $this->color . "<br>";
      echo "植物的年龄" . $this->age . "<br>";
}
public function __destruct_destruct() {
     echo "再见:" . $this->name. "<br>";
}
 }
$Carnation=new plants('康乃馨','粉色',4);
$Carnation->getInfo();
$rose=new plants('玫瑰','红色',6);
$rose->getInfo();
$Campanula=new plants('风铃草','白色',8);
$Campanula->getInfo();
?>