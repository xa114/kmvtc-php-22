<!DOCTYPE html>
<html>
<head>
    <title>PHP实现简单计算器</title>
    <meta charset="UTF-8">
</head>
<body>
    <tablealign="center"border="1" width="500">
        <caption><h1>caption我的计算器</h1></caption>
        <form action="">
            <tr>
                <td>
                    <input type="text" size="5" name="num1" value="<?php echo $_GET["num1"] ?? ''; ?>">
                </td>
                <td>
                    <select name="ysf">
                        <option value="+" <?php echo ($_GET["ysf"] ?? '') === "+" ? "selected" : ""; ?>>+</option>
                        <option value="-" <?php echo ($_GET["ysf"] ?? '') === "-" ? "selected" : ""; ?>>-</option>
                        <option value="x" <?php echo ($_GET["ysf"] ?? '') === "x" ? "selected" : ""; ?>>x</option>
                        <option value="/" <?php echo ($_GET["ysf"] ?? '') === "/" ? "selected" : ""; ?>>/</option>
                        <option value="%" <?php echo ($_GET["ysf"] ?? '') === "%" ? "selected" : ""; ?>>%</option>
                    </select>
                </td>
                <td>
                    <input type="text" size="5" name="num2" value="<?php echo $_GET["num2"] ?? ''; ?>">
                </td>
                <td><input type="submit" name="sub" value="计算"></td>
            </tr>
            <?php if (isset($_GET["sub"])): ?>
            <tr><td colspan="4">
            <?php
                $num1 = $_GET["num1"] ?? 0;
                // echo "用户输入的第1个值是",$num1;
                $num2 = $_GET["num2"] ?? 0;
                // echo "用户输入的第2个值是",$num2;
                $message = "";
                if (is_numeric($_GET["num1"]) && is_numeric($_GET["num2"])) {
                    $num1 = (float)$num1;
                    $num2 = (float)$num2;

​                    $result = 0;
​                    switch ($_GET["ysf"]) {
​                        case '+': $result = $num1 + $num2; break;
​                        case '-': $result = $num1 - $num2; break;
​                        case 'x': $result = $num1 * $num2; break;
​                        case '/': 
​                            if ($num2 != 0) {
​                                $result = $num1 / $num2;
​                            } else {
​                                $message = "除数不能为0。";
​                                echo $message;
​                            }
​                            break;
​                        case '%': 
​                            if ($num2 != 0) {
​                                $result = $num1 % $num2;
​                            } else {
​                                $message = "除数不能为0。";
​                                echo $message;
​                            }
​                            break;
​                    }
​                    echo  "结果是：" . $result;
​                } else {
​                    $message = "请输入有效的数字。";
​                    echo $message;
​                }
​            ?>
​            </td></tr>
​            <?php endif; ?>
​        </form>
​    </table>
</body>
</html>