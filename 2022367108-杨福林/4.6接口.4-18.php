<?php
header( "Content-Type:test/html;charsrt=utf-8");
interface animal {
    function run();
    function shout();
}
interface landanimal {
    public function liveonland();
}
class dog implements animal, landanimal {
    public function run() {
        echo"小狗在奔跑<br>";
    }
    public function shout() {
        echo"汪汪……<br>";
    }
    public function liveonland() {
        echo"小狗在陆地上生活<br>";
    }
}
$dog=new dog();
$dog->run();
$dog->shout();
$dog->liveonland();
?>