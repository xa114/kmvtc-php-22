<?php
class vegetables  {
    public $name;
    public $color;
    public $taste;
    
    // 构造函数
    public function __construct( $name, $color, $taste) {
            $this->name=$name;
            $this->color=$color;
            $this->taste=$taste;
    }

    
    public function getInfo() {
        echo "蔬菜名称：" . $this->name . "<br>";
        echo "蔬菜颜色: " . $this->color . "<br>";
        echo "蔬菜味道: " . $this->taste . "<br>"; 
    }

    // 析构函数
    public function __destruct(){
        echo "再见:" . $this->name. "<br>";

    }
}

$potato=new vegetables('土豆','黄色','多种多样');
$potato->getInfo();
$balsampear=new vegetables('苦瓜','绿色','苦');
$balsampear->getInfo();
$eggplant=new vegetables('茄子','紫色','辣');
$eggplant->getInfo();
 //类的继承
 class animal {
    public $name;
    public $color;
    public $climate;
    public function_construct($name, $color, $age) {
        $this->name= $name;
        $this->color=$color;
        $this->age=$climate;
        }
  public function getInfo(){
       echo "植物名称:" . $this->name. "<br>";
       echo "植物颜色:" . $this->color ."<br>";
       echo "植物气候:" . $this->climate."<br>";
  }
}

/**
*定义一个bird类，使用extends关键字来继承animal类，作为animal类的子类
*/
class grass extends animal {
   public $wing;
   public function fly() {
      echo 'I can fly!!!';
      }
  }
$crow=new grass ("小草" , "绿色" , "温带");
$crow->getInfo();
$crow->fly();




//类的重载
class animal {
    public $name;
    public $color;
    public $climate;
    public function_construct($name, $color, $age) {
        $this->name= $name;
        $this->color=$color;
        $this->age=$climate;
        }
  public function getInfo(){
       echo "植物名称:" . $this->name. "<br>";
       echo "植物颜色:" . $this->color ."<br>";
       echo "植物气候:" . $this->climate."<br>";
  }
}
//*
*定义一个bird类,使用extends关键字来继承animal类,作为animal类的子类
class tree extends animal {
   public $wing;
   public function gerInfo() {
     parent::getInfo();
     echo "植物有" . $this->wing . '藤蔓<br>'
     $this->fly();
  }
  public function fly() {
    echo"我有藤蔓！！！";
    }
  public function_construct($name, $color, $age , $swing){
      parent::__construct($name, $color, $age);
         $this->wing=$wing;
         }
  }
$crow=new tree("树", "棕色","热带", "丑陋的");
$crow->getInfo();


//封装
header ("Content-Type:text/html;charset=utf-8");
class animal {
    public $name;
    public $color;
    public $climate;
    public function_construct($name, $color, $age) {
        $this->name= $name;
        $this->color=$color;
        $this->age=$climate;
        }
  public function getInfo(){
       echo "植物名称:" . $this->name. "<br>";
       echo "植物颜色:" . $this->color ."<br>";
       echo "植物气候:" . $this->climate."<br>";
  }
}
$rose=new animal("玫瑰", "红色","亚热带");
//$rose->getInfo();
echo $rose->name;


