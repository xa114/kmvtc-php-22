<?php
$a="hello";
echo gettype($a)."<br>"; //输出结果：string
$b=array(1,2,5);
echo gettype($b)."<br>"; //输出结果：array
echo intval(4.5)."<br>"; //输出结果：4
var_dump($a);            //输出结果：string(5)"Hello"
echo "<br>";
var_dump($a,$b);
//输出结果：string(5)"Hello" array(3) {[0]=>int(1) [1]=>int(2) [2]=>int(5)}

echo(floor(0.60)); //输出结果：0
echo(floor(0.40)); //输出结果：0
echo(floor(5));    //输出结果：5
echo(floor(5.1));  //输出结果：5
echo(floor(-5.1)); //输出结果：-6
echo(floor(-5.9)); //输出结果：-6

echo rand();
echo rand();
echo rand(10,100);

$pizza = "piece1 piece2 piece3 piece4 piece5 piece6";
$pieces = explode(" ",$pizza);
echo $pieces[0]; //输出结果：piece1
echo $pieces[1]; //输出结果：piece2

echo md5("apple");
//输出结果:

var_dump(checkdate(12,31,2000)); //bool(true)
var_dump(checkdate(2,29,2001));  //bool(false)

date_default_timezone_set("PRC");       //设置时区
var_dump(checkdate(12,31,2000))."<br>"; //bool(true)
var_dump(checkdate(2,29,2000))."<br>";  //bool(false)
echo time()."<br>";                     //返回当前时间截
echo mktime(0,0,0,12,25,2016)."<br>";   
echo "今天距2030年国庆节还有".ceil(mktime(0,0,0,10,1,2030)-time())/(24*60*60)."天<br>";
//今天距2030年国庆节还有多少天
echo "现在是:".date('Y-m-d H:i:s');     //date()返回格式化的当地时间


function random_text($count, $rm_similar = false) {                                   //返回给定长度的随机字符串
    $chars = array_flip(array_merge(range(0,9),range('A','Z')));                      //创建字符数组
    if($rm_similar) {
        unset($chars[0], $chars[1], $chars[2], $chars['I'], $chars['O'], $chars['Z']);
    }
    //删除容易引起混淆的相似单词
    //生成随机字符文本
    for($i = 0, $test =''; $i < $count; $i++){
        $text.=array_rand($chars);
    }
    return $text;
}

//函数测试test_function.php
include 'functions.php';
echo random_text(5,true);

include 'funtions.php';
//包含给定长度字符串的自定义函数function.php
if(! isset($_SESSION)) {
    session_start();
}
//开启或继续会话，保持图片验证码到会话中供其他页面调用
$width = 65;
$height = 20;
$image = imagecreate( $width, $height);
//创建65pxx20px大小的图像
$bg_color = imagecolorallocate($imgre,0x33,0x66,0xff);
//为一幅图像分配颜色：imagecolorallocate
$text = random_text(5);
//取得随机字符串
$font = 5;
$x = imagesx($image) / 2 - strlen($text) * imagefontwidth($font) /2;
$y = imagesx($image) / 2 - imagefontwidth($font) /2;
//定义字体.位置
$fg_color = imagecolorallocate($imgre,0xff,0xff,0xff);
$imgestring( $image, $font, $x, $y, $text, $fg_color);
//输出字符到图形上
$_SESSION['captcha'] = $text;
//保证验证码到会话，用于比较验证
$header('Content-type:image/png');
$imagepng($image);
$imagedestroy($image);
//输出图像

header("Content-type:image/png");           //设置生成图像格式
$im = imagecreate(120,30);                  //新建画布
$bg = imagecolorallocate( $im,0,0,225);     //背景
$sg = imagecolorallocate( $im,225,225,225); //前景
imagefill( $im,120,30,$bg);                 //填充背景
imagestring( $im,7,8,5,"image create",$sg); //填充字符串
imagepng( $im);                             //输出图形
imagedestroy( $im);                         //销毁资源变量

header("Content-type:image/png");
$im = imagecreate(80,20);
$bg = imagecolorallocate( $im,225,225,0);
$sg = imagecolorallocate( $im,0,0,0);
$ag = imagecolorallocate( $im,231,104,50);
imagefill( $im,120,30,$bg);
$str = 'qwertyuiopasdfghjklzxcvbnm123456789QWERTYUIOPASDFGHJKLZXCVBNM';
$len = strlen($str)-1;
for( $i=0;$i<4;$i++) {
$str1=$str[mt_rand(0,$len)];    //取随机字符串
imagechar( $im,7,16 * $i+7,2,$str1,$sg);
}
for( $i=0;$i<100;$i++) {        //填充杂色点
    imagesetpixel( $im,rand()%80,rand()%20,$ag);
    }
imagepng( $im);
imagedestroy($im);
?>
