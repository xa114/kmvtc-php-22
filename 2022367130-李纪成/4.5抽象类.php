<?php
abstract class animal //使用abstract关键字声明一个抽象类
{
    abstract public function shout(); //在抽象类中声明抽象方法
}
class dog extends animal //定义dog类继承animal类
{
    public function shout() //实例抽象方法
    {
        echo "汪……<br>";
    }
}
class cat extends animal //定义cat类继承animal类
{
    public function shout() //实例抽象方法
    {
        echo "喵……<br>";
    }
}
$dog=new dog(); //实例化dog类
$dog->shout();  //调用dog类的shout方法
$cat=new cat(); //实例化cat类
$cat->shout();  //调用cat类的shout方法
?>