<?php
class vegetable {  //创建vegetable类
    //成员属性
    public $name;  //成员属性name
    public $color;
    public $taste;
//构造方法（功能：创建对象时为成员属性赋初值，完成初始化工作 ）
public function __construct ( $name, $color, $taste) {
    //echo "我是构造方法"；
    $this->name = $name;
    $this->color= $color;
    $this->taste= $taste;
}
//成员的属性 （名字、颜色、味道）
public function getInfo() {
    echo "蔬菜的名字:" . $this->name . "<br>";  //(.).字符串拼接
    echo "蔬菜的颜色:" . $this->color . "<br>";
    echo "蔬菜的味道:" . $this->taste . "<br>";
}

}
//通过new关键字实例化出一个对象 名称为 tomato
$tomato=new vegetable("西红柿","红色","酸酸甜甜");
$tomato->getInfo();

//通过new关键字实例化出一个对象 名称为 bitter
$bitter=new vegetable("苦瓜","绿色","苦");
$bitter ->getInfo();

//通过new关键字实例化出一个对象 名称为 corn
$corn =new vegetable("玉米","黄色","甜");
$corn ->getInfo();

// 如何创建一个对象？
$pig = new animal("名字","颜色","年龄");

?>