<?

// 【布尔型】定义一个变量a，赋值为false
$a = false;
// 【字符串】定义一个变量username，赋值为“Mike”
$username = "Mikes";
// echo命令打印，显示
echo "the username is  ", $username, "<br>";
// 打印你好，今天是星期四
// echo "<h1>你\a好\n\r今天是\\疯狂星期四,v我\$50</h1><br>";

$a = 10;
echo "a的值是$a<br>";
echo "a的值是\$a<br>";
// ==是作比较
// = 赋值
if ($username == "Mike") {
    echo "Hi, Mike <br>";
    # code...
} else {
    echo "you are not Mike<br>";

}


if ($a == true) {
    echo "a is true";
} else {
    echo "a is false";
}

echo "<br>";
//整型
$n1 = 123;
echo "n1=", $n1, "<br>";
$n2 = 0;
echo "n2=", $n2, "<br>";
$n3 = -123;
echo "n3=", $n3, "<br>";
$n4 = 0123;
echo "n4=", $n4, "<br>";
$n5 = 0xFF;
echo "n5=", $n5, "<br>";
$n6 = 0b11111111;
echo "n6=", $n6, "<br>";
//浮点型
$pi = 3.1415926;
echo "pi=", $pi, "<br>";
$width = 3.3e4;
echo "width=", $width, "<br>";
$var = 3e-2;
echo "var=", $var, "<br>";

echo 3 ** 4;
// 数组
$arr1 = array(1, 2, 3, 4, 5, 6, 7, 8);
$arr2 = array("animals" => "dog", "color" => "red");
// 格式化代码 alt+shift+F
echo "<br>", $arr1[0];//1
echo "<br>", $arr1[7];//1
echo "<br>", $arr2["color"];//1

//=====================================================
// 对象
class Animal
{
    //基本属性
    var $name;
    var $age;
    var $weight;
    var $sex;
    function __construct($name)
    { // 构造函数
        $this->name = $name; // 将传入的参数赋值给属性 name
    }
    //基本方法
    public function run()
    {
        echo "haha,", $this->name, " is running<br>";
    }
    public function eat()
    {
        echo "haha,i am eating";
    }
}
$dog = new Animal("dog");
$cat = new Animal("cat");
$dog->run();
$cat->run();

// 定义一个英雄类
class hero
{
    //血量
    public $hp = 100;
    //姓名
    public $name;
    //攻击力
    public $attack;

    public $shield = 0.2;

    function __construct($name, $attack)
    {
        $this->name = $name;
        $this->attack = $attack;
    }

    // 攻击
    function attack($hero)
    {
        echo $this->name, "攻击了", $hero->name,"<br>";
        $hero->hp -= $this->attack;

        $hero->hp = $hero->hp - $this->attack*(1-$hero->shield);
        echo $hero->name,"的剩余血量：", $hero->hp,"<br>";
    }
}

$hero1 = new hero("瑟提",6);
$hero2 = new hero("鲁班",10);

$hero1->attack($hero2);
$hero2->attack($hero1);
$hero2->attack($hero1);
$hero2->attack($hero1);
$hero2->attack($hero1);
?>


<?



$a=false;
$username ="Mkies";
echo  "the username  is",$username ,"<br>";
echo "<h1>你好， 今天是疯狂星期四</h1><br>";
if ($username ==  "Mike"){
echo  "Hi, Mkie <br>";
   #code...
}else{
echo "you are not mike<br>";

}
if (condition){
    #code....
}elseif (condition){
    #code...
}elseif (condition){
    #code...
}
//echo $username == "mtimi";
//echo $username == "mike";

if ($a == true){
    echo "a is true";
}else {
    echo "a is  false";
}



hpinfo()
?>



<?php
       $i =$j = 0;
       echo '<table>';
       for ( $i =  1; $j =9; $j++){
        echo'<tr>';
        for ($J = 1; $j <= $i; $j++){
            echo "<td>". getCN($j). getCN($i). ($i * $j<10 ?'得' ：”). getCN($i *  $j). '</td>';
        }
        echo'</tr>';
       }
       echo'<table>';
       function getCN($mum){
        $cns = array('零','一','二','三','四','五','六','七','八','九','十',);
        if ($num < 10){
            return $cns[$num];
        }
        elseif ($num<100) {//只对小于100的值进行计算
           $r = str_split($num);
           $ln = $cns[ $r[0]] . $cns[10];
           if( $r[1]>0) {
                 $ln.= $cns[ $r[1]];

           }
             return $ln;

        } else{
              return $num;

        }
       }
       ?>                    

       <?php
//gettype()
$a = "Hello";
echo gettype($a) . "<br>";   //输出结果：string
$b = array(1 , 2, 5);
echo gettype ($b) ."<br>";
//intval
echo intval(4.5) . "<br>";
//var_dump
var_dump($a);
echo"<br>";
var_dump($a ,$b);
//输出结果：string(5) "Hello" array(3) {[0] ->int(1) [1] => int(2) [2]=>int(5)}
?>


<?php
echo(floor(0.60)); //输出结果：0
echo(floor(0.40)); //输出结果：0
echo(floor(5));  //输出结果：5
echo(floor(5.1));  //输出结果：5
echo(floor(-5.1));  //输出结果：-6
echo(floor(-5.9));  //输出结果：-6
?>

<?php
//rand()函数，返回随机整数
echo rand();
echo rand();
echo rand(10,100);
?>

<?php
$pizza = "piece1 piece2 piece3 piece4 piece5 piece6";
//explode()函数将字符串依指定的字符串或者字符separator切开
$pieces = explode(" ", $pizza);
echo $pieces[0];  //输出piece1
echo $pieces[1];  //输出piece2
?>


<?php
echo md5("apple"); //输出结果：1f3870be274f6c49b3e31a0c6728957f
?>


<?php
//checkdate()函数 验证日期的有效性，日期有效则返回Ture,否则返回False
var_dump(checkdate(12,31,2000));  //bool(true)
var_dump(checkdate(2,29,2001));   //bool(true)
?>

<?php
//设置时区
date_default_timezone_set("PRC");
//checkdate()
var_dump(checkdate(12,31,2000)) . "<br>"; 
var_dump(checkdate(2,31,2000)) . "<br>";
//mktime()
echo time() . "<br>";
//mktime()函数（用于返回一个日期的UNIX时间戳，通过date（）函数来对其进行格式化
echo mktime (0,0,0,12,25,2016) ."<br>";
//今天距离2023年国庆还有多少天
echo "今天距离2023年国庆还有多少天" . ceil ((mktime(0,0,0,10,1,2030)-time())/(24*60*60)) . "天<br>";
//date()返回格式化的当地时间
echo "现在是：" . date('Y-m-d H:i:s');
?>



