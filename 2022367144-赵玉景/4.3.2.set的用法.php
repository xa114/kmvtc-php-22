<?php
header("Content-Type:text/html;charset=utf-8");
class sanrio {
    private $name; //私有的成员属性
    private $color;
    private $age;
    //__get()方法用来获取私有属性的值
    public function__get( $property_name) {
        if(isset( $this->$property_name)) {
            return $this->$property_name;
        } else {
            return(NULL);
        }
    }
    //__set()方法用来设置私有属性的值
    public function __set( $property_name, $value) {
        $this->$property_name=$value;
    }
}
$Cinnamoroll=new sanrio();
$Cinnamoroll->name="玉桂狗";  //自动调用__set()方法
$Cinnamoroll->color="白色";
$Cinnamoroll->age=5;
echo $Cinnamoroll->name. "<br>";//自动调用__get()方法
echo $Cinnamoroll->color. "<br>";
echo $Cinnamoroll->age. "<br>";
?>


