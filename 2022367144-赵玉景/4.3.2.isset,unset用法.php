<?php
class sanrio {
    private $name; //私有的成员属性
    private $color;
    private $age;
    //__get()方法用来获取私有属性的值
    function __get( $property_name) {
        if (isset( $this->$property_name)) {
            return ($this->$property_name);
        } else {
            return(NULL)
        }
    }
    //__set()方法用来设置私有属性
    function __set( $property_name, $value) {
        $this->$property_name=$value;
    }
//__isset()方法
function __isset( $property_name) {
    return isset( $this->$property_name);
}
//__unset()方法
function __unset( $property_name) {
    unset( $this->$property_name);
}
}
$Cinnamoroll=new sanrio();
$Cinnamoroll->name="玉桂狗"; 
echo var_dump(isset( $Cinnamoroll->name)) . "<br>";
echo $Cinnamoroll->name . "<br>";
unset( $Cinnamoroll->name);
echo $Cinnamoroll->name;
?>