+        4.4.1 static 关键字
<?php
class animal {
    private $name;
    private $color;
    private $age;
    private static $sex="雄性";//私有的静态属性
    //静态成员方法
    public static function getInfo() {
        echo "动物是雄性的。";
    }
}
//echo animal::$sex;           //错误，静态属性私有，外部不能访问
animal::getInfo();             //调用静态方法



echo "==============================================="."<br>";
header("Content-Type:text/html;charset=utf-8");
class animal{
    private $name;
    private $color;
    private $age;
    private static $sex="雄性";//私有的静态属性
    public function __construct($name,$color,$age) {
        $this->name=$name;
        $this->color=$color;
        $this->age=$age
    }
    public function getInfo() {
        echo "动物的名称:" . $this->name . "<br>";
        echo "动物的颜色:" . $this->color . "<br>";
        echo "动物的年龄:" . $this->age . "<br>";
        //echo "动物的性别:" . $this->sex . "<br>";
        self::getSex();
    }
    private static function getSex() {
        echo"动物的性别:" . self::$sex;
    }
}
$dog=new animal("小狗","黑色",4);
$dog->getInfo();



echo "==============================================="."<br>";
//4.4.2 final关键字
final class animal {}
class bird extends animal {}

class animal{
    final public function getInfo() {
    }
}
class bird extends animal{
    public function getInfo(){

    }
}
$crow=new bird();
$crow->getInfo();

echo "==============================================="."<br>";
//4.4.3 self关键字
class animal {
    private static $firstCry=0;//私有的静态变量
    private $lastCry;
    //构造方法
    function __construct() {
        $this->lastCry=++self::$firstCry;
    }
    function printLastCry() {
        var_dump($this->lastCry);
    }
}
$brid=new animal();//实例化对象
$brid->printLastCry();//输出:int(1)

echo "==============================================="."<br>";
//4.4.4  const关键字
class MyClass {
    //定义一个常量
    const constant='我是个常量!';
    function showConstant() {
        echo self::constant."<br>";//类中访问常量
    }   
    
}
echo MyClass::constant."<br>";//使用类名来访问常量
$class=new MyClass();
$class->showConstant();

echo "==============================================="."<br>";
//4.4.5 __toString方法
class TestClass {
    public $foo;
    public function __construct( $foo) {
        $this->foo=$foo;
}
//定义一个__toString()方法，返回一个成员属性$foo
public function __toString() {
    return $this->foo;
  }
}
$class=new TestClass('Hello');
//直接输出对象
echo $class;     //输出结果为'Hello'


echo "==============================================="."<br>";
//4.4.6 __clone()方法
class animal {
    private $name;     //成员属性name
    private $color;
    private $age;
    public function __construct( $name, $color, $age ) {
        $this->name=$name;
        $this->color=$color;
        $this->age=$age;
    }
    public function getInfo() {
        echo'名字：'. $this->name . '颜色:'. $this->color .'年龄:'. $this->age .'<br>';
}
public function __clone() {
    $this->name = '狗';
    $this->color = '黑';
    $this->age = '2岁';
    }
}
$pig=new anmial('猪','白色','1岁');
$pig2=clone $pig;       //克隆对象
$pig2->getInfo();        

echo "==============================================="."<br>";
//4.4.7 __call()方法
class Test {
    function __call($function_name, $args) {
    print"你所调用的函数:$function_name(参数:";
    print_r($args);
    print")不存在!<br>";
    }        
   }
   $test=new Test();
   $test->demo("one","two","three");


   echo "==============================================="."<br>";
   //4.4.8自动加载类
   spl_autoload_register(function ($class_name) {
    require_once $class_name . '.class.php';
   });
   $obj = new MyClass1();
   print_r($obj);
   $obj2 = new MyClass2();
   print_r($obj2);
   ?>