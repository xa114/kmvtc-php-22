<?php
header("Content-Type:text/html;charset=utf-8");
//定义接口
interface animal{
    //声明抽象方法
    function run();
    function shout();
}
//定义dog类，实现animal接口
class dog implements animal {
    //实现接口中的抽象方法
    public function run() {
        echo "小狗在奔跑<br>";
    }
    public function shout() {
        echo "汪汪······<br>";
    }
}
//定义cat类,实现animal接口
class cat implements animal {
    //实现接口中的方法
    public function run(){
        echo "小猫在奔跑<br>";
    }
    public function shout(){
        echo "喵喵······<br>";
    }
}
$dog=new dog();
$dog->run();
$dog->shout();
$cat=new cat();
$cat->run();
$dog->shout();

echo "==============================================="."<br>";
header("Content-Type:text/html;charset=utf-8");
interface animal {
    function run();
    function shout();
}
interface landanimal {
    public function liveonland();
}
class dog implements animal, landanimal{
    public function run(){
        echo "小狗在奔跑<br>";
    }
    public function shout(){
        echo "小狗在陆地生活<br>";
    }
}
$dog=new dog();
$dog->run();
$dog->shout();
$dog->liveonland();
?>