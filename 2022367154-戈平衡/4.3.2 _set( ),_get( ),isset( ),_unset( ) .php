//4.3.2.1 get  (取值)
    public function __get($property_name){
        // 检查属性 this-> 当前类  的 property_name 属性 是否存在
        if(isset($this->$property_name)){
            return (this->$property_name);
        }else {
            return(NULL);
        }
    }


    //4.3.2.2 set  (赋值)
    public __set($property_name,$value){
        $this->$property_name = $value;
    }
//下面的代码说明了其含义和作业
    <?php
header("content-type:text/html;charset=utf-8");
class animal {
    private $name; //私有的成员属性
    private $color;
    private $age;
    //_get( )方法用来获取私有属性
    public function _get( $property_name,$value) {
        if (isset( $this->$property_name)) {
            return $this-> $property_name;
        } else {
            return(NULL);
        }
    }
    //_set（）方法用来设置私有属性
    public function __set( $property_name, $value) {
        $this-> $property_name= $value;
    }
}
$tiger=new animal();
$tiger->name="老虎"; //自动调用_set()方法
$tiger->color="花花绿绿";
$tiger->age=2;
echo $tiger->name."<br>";
echo $tiger->color."<br>";
echo $tiger->age."<br>";
?>


    //4.3.2.3 isset  (检查)
    private function __isset($property_name) {
        return isset($this->$property_name);
    }
//下面的程序代码说明了 _isset()和_unset()两个方法的含义和作用
<?php
class animal {
    private $name; //私有的成员属性
    private $color;
    private $age;
//_get( )方法用来获取私有属性
    public function _get($property_name,$value) {
        if (isset( $this->$property_name)) {
            return $this-> $property_name;
        } else {
            return(NULL);
        }
    }
//_set（）方法用来设置私有属性
    public function __set( $property_name, $value) {
        $this-> $property_name= $value;
    }
//_isset()方法
function __isset($property_name) {
    return isset($this->$property_name);
}

//_unset()方法
function __unset($property_name) {
    unset( $this->$property_name)
 }
}
$fish=new animal();
$fish->name="鲤鱼";
echo var_dump(isset( $fish->name)) ."<br>";
echo $fish->name . "<br>";
unset( $fish->name);
echo $fish->name;
?>

    //4.3.2.4 unset  (删除)
    private function __unset($property_name) {
        return unset($this->$property_name);
    }
$panda = new animal();

/**
 * private[私有的]
 * 1、仅针对当前class享有访问权限
 * 2、当前类的子类不允许访问
 * 3、类的外部均不允许访问
 */

 //!! __get __set 方法写了以后就会自动对private属性取值或者赋值

 // todo 需要同学测试 将__get __set 方法注释以后运行的结果和不注释运行的结果，作比较
$panda->name="熊猫";
$panda->color="黑白";
$panda->age=12;

echo $panda->name;
echo $panda->color;
echo $panda->age;

//todo 在类的外部使用isset函数来判断实体的变量是否设定


//4.3.2.4 unset 测试用例
echo $panda->name;//"熊猫";
unset($panda->name);
echo $panda->name;// 空输出
