//4.5 抽像类
<?php

header("content-type:text/html;charset=utf-8");
//使用abstract关键字声明一个抽象类
abstract class animal {
    //在抽象类中声明抽象方法
    abstract public function shout();
}
class dog extends animal{
    public function shout() {
        //定义dog类继承animal类
        echo "嗷嗷.......<br>";
    }
}
//定义cat类继承animal
class cat extends animal{
    //实现抽象方法
    public function shout() {
        echo "咔咔......<br>";
    }
}

Sdog=new dog();
$dog->shout();
$cat=new cat();
$cat->shout();
?>