<?php
// 设置题目中给定的数据
$legsPerRabbit = 4; // 兔的腿数
$legsPerChicken = 2; // 鸡的腿数

// 获取用户输入的头和脚的数量
$heads = intval($_POST['heads']);
$feet = intval($_POST['feet']);

// 计算总共的脚数
$totalFeet = $heads * 2 + $feet;

// 初始化兔和鸡的数量
$rabbits = 0;
$chickens = 0;

// 根据头和脚的数量计算兔和鸡的数量
while (true) {
// 假设所有动物都是鸡，计算总共的腿数
$totalLegs = $chickens * $legsPerChicken + $rabbits * $legsPerRabbit;

// 如果总共的腿数等于总共的脚数，则猜测正确
if ($totalLegs == $totalFeet) {
echo "恭喜你猜对了！鸡的数量为 {$chickens}，兔的数量为 {$rabbits}。";
break;
} else {
// 如果总共的腿数小于总共的脚数，则增加兔的数量
if ($totalLegs < $totalFeet) {
$rabbits++;
} else {
// 如果总共的腿数大于总共的脚数，则增加鸡的数量
$chickens++;
        }
    }
    }

<!DOCTYPE html>
<html>
<head>
<title>鸡兔同笼问题</title>
</head>
<body>
<h1>鸡兔同笼问题</h1>
<form method="post">
<label for="heads">请输入头的数量：</label>
<input type="number" name="heads" id="heads" required><br>
<label for="feet">请输入脚的数量：</label>
<input type="number" name="feet" id="feet" required><br>
<input type="submit" value="计算">
</form>
</body>
</html>