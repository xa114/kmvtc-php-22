<?php
class animal 
{
    public $name;
    public $color;
    public $age;
    public function __construct($name,$color,$age)
    {
        $this->name=$name;
        $this->color=$color;
        $this->age=$age;
    }
    public function getInfo() {
        echo"动物的名称:" . $this->name . "<br>";
        echo"动物的颜色:" . $this->color . "<br>";
        echo"动物的年龄:" . $this->age . "<br>";
    }
}
/**定义一个fish类，使用extends来继承animal类，作为animal的子类
*/
class fish extends animal 
{
    //fish自有属性$swim
    public $swim;
    public function getInfo() 
    {
        parent::getInfo();
        echo"鱼类有" . $this->swim . '鱼鳍<br>';
        $this->swim();
    }
    public function swim() 
    {
        echo"我会游泳!!!"
    }
    public function __construct($name,$color,$age,$environment) 
    {
        parent::__construct($name,$color,$age);
        $this->wing=$wing;
    }
}
$shark=new fish("鲨鱼","灰色","4","海水");
$shark->getInfo();
$carp=new fish("鲤鱼","银色","4","淡水");
$carp->getInfo();
?>